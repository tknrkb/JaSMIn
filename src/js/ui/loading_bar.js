/**
 * The LoadingBar class definition.
 *
 * The LoadingBar provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.LoadingBar');

goog.require('JaSMIn.UI');
goog.require('JaSMIn.GameLogLoader');



/**
 * LoadingBar Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Panel}
 * @param {!JaSMIn.GameLogLoader} gameLogLoader the game log loader instance
 */
JaSMIn.UI.LoadingBar = function(gameLogLoader) {
  goog.base(this, 'jsm-loading-bar');

  /**
   * The game log loader instance.
   * @type {!JaSMIn.GameLogLoader}
   */
  this.gameLogLoader = gameLogLoader;

  /**
   * The progress label.
   * @type {!Element}
   */
  this.progressBar = document.createElement('div');
  this.progressBar.style.width = '0px';
  this.domElement.appendChild(this.progressBar);

  /**
   * The progress label.
   * @type {!Element}
   */
  this.label = document.createElement('span');
  this.label.innerHTML = '0 / 0 KB';
  this.domElement.appendChild(this.label);


  // By default hide the loading bar
  this.setVisible(false);



  /** @type {!Function} */
  this.handleLoadStartListener = this.handleLoadStart.bind(this);
  /** @type {!Function} */
  this.handleLoadProgressListener = this.handleLoadProgress.bind(this);
  /** @type {!Function} */
  this.handleLoadEndListener = this.handleLoadEnd.bind(this);

  // Add game log loader event listeners
  this.gameLogLoader.addEventListener(JaSMIn.GameLogLoaderEvents.START, this.handleLoadStartListener);
  this.gameLogLoader.addEventListener(JaSMIn.GameLogLoaderEvents.PROGRESS, this.handleLoadProgressListener);
  this.gameLogLoader.addEventListener(JaSMIn.GameLogLoaderEvents.FINISHED, this.handleLoadEndListener);
  this.gameLogLoader.addEventListener(JaSMIn.GameLogLoaderEvents.ERROR, this.handleLoadEndListener);
};
goog.inherits(JaSMIn.UI.LoadingBar, JaSMIn.UI.Panel);






/**
 * GameLogLoader->"start" event listener.
 * This event listener is triggered when loading a new game log file has started.
 *
 * @param {!Object} evt the event object
 */
JaSMIn.UI.LoadingBar.prototype.handleLoadStart = function(evt) {
  // Reset labels and progress bar
  this.progressBar.style.width = '0%';
  this.label.innerHTML = '0 / 0 MB';

  // Ensure loading bar is visible
  this.setVisible(true);
};



/**
 * GameLogLoader->"progress" event listener.
 * This event listener is triggered when new data was received.
 *
 * @param {!Object} evt the event object
 */
JaSMIn.UI.LoadingBar.prototype.handleLoadProgress = function(evt) {
  this.setProgress(100 * evt.loaded / evt.total, evt.loaded / 1000000, evt.total / 1000000);
};



/**
 * GameLogLoader->"finished"|"error" event listener.
 * This event listener is triggered when loading a new game log file has terminated.
 *
 * @param {!Object} evt the event object
 */
JaSMIn.UI.LoadingBar.prototype.handleLoadEnd = function(evt) {
  // Hide loading bar on load end
  this.setVisible(false);
};



/**
 * The callback triggered when a new replay file is loaded.
 *
 * @param  {!number} percent the loaded percentage
 * @param  {!number} loadedMB the MBs loaded
 * @param  {!number} totalMB the total MBs to load
 * @return {void}
 */
JaSMIn.UI.LoadingBar.prototype.setProgress = function(percent, loadedMB, totalMB) {
  this.progressBar.style.width = '' + percent.toFixed(1) + '%';
  this.label.innerHTML = '' + loadedMB.toFixed(3) + ' / ' + totalMB.toFixed(3) + ' MB';
};



