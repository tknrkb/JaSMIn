/**
 * The Overlay class definition.
 *
 * The Overlay abstracts
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.Overlay');

goog.require('JaSMIn.UI');



/**
 * Overlay Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Panel}
 * @param {!string=} className the css class string of the inner panel
 * @param {!boolean=} hideOnClick true if clicking on inner panel should cause the overlay to close, false if not (default: false)
 */
JaSMIn.UI.Overlay = function(className, hideOnClick) {
  goog.base(this, 'overlay full-size');

  /**
   * The inner overlay panel.
   * @type {!Element}
   */
  this.innerElement = JaSMIn.UI.createDiv(className);
  this.domElement.appendChild(this.innerElement);

  var scope = this;

  /** @param {!Event} event */
  var hideOverlay = function(event) {
    scope.setVisible(false);
    event.stopPropagation();
  };

  // Add mouse and touch listener
  this.domElement.addEventListener('mousedown', hideOverlay);
  this.domElement.addEventListener('ontouchstart', hideOverlay);

  if (!hideOnClick) {
    this.innerElement.addEventListener('mousedown', JaSMIn.UI.StopEventPropagationListener);
    this.innerElement.addEventListener('ontouchstart', JaSMIn.UI.StopEventPropagationListener);
  }

  this.setVisible(false);
};
goog.inherits(JaSMIn.UI.Overlay, JaSMIn.UI.Panel);
