goog.provide('JaSMIn.UI.Panel');

goog.require('JaSMIn.UI');



/**
 * Panel Constructor
 *
 * @constructor
 * @param {!string=} className the css class string
 */
JaSMIn.UI.Panel = function(className) {
  /**
   * The component root element.
   * @type {!Element}
   */
  this.domElement = JaSMIn.UI.createDiv(className);

  /**
   * Visibility change listener
   * @type {!Function | undefined}
   */
  this.onVisibilityChanged = undefined;
};



/**
 * Add (append) the given element to the panel.
 *
 * @param {!Element} element the element to add/append
 */
JaSMIn.UI.Panel.prototype.appendChild = function(element) {
  this.domElement.appendChild(element);
};



/**
 * Set this component visible or invisible.
 *
 * @param {!boolean=} visible true for visible, false for invisible
 */
JaSMIn.UI.Panel.prototype.setVisible = function(visible) {
  if (visible === undefined) {
    visible = true;
  }

  var isVisible = JaSMIn.UI.isVisible(this.domElement);

  if (isVisible !== visible) {
    JaSMIn.UI.setVisibility(this.domElement, visible);
    if (this.onVisibilityChanged) {
      this.onVisibilityChanged(this);
    }
  }
};



/**
 * Toggle visibility of panel.
 */
JaSMIn.UI.Panel.prototype.toggleVisibility = function() {
  var newVal = !JaSMIn.UI.isVisible(this.domElement);

  JaSMIn.UI.setVisibility(this.domElement, newVal);
    if (this.onVisibilityChanged) {
      this.onVisibilityChanged(this);
    }
};



/**
 * Check if this component is currently visible.
 *
 * @return {!boolean} true for visible, false for invisible
 */
JaSMIn.UI.Panel.prototype.isVisible = function() {
  return JaSMIn.UI.isVisible(this.domElement);
};
