goog.provide('JaSMIn.UI.PanelGroup');

goog.require('JaSMIn.UI');
goog.require('JaSMIn.UI.Panel');


/**
 * PanelGroup Constructor
 *
 * @constructor
 */
JaSMIn.UI.PanelGroup = function() {
  /**
   * The list of panels in this group.
   * @type {!Array<!JaSMIn.UI.Panel>}
   */
  this.panels = [];


  /**
   * The currently active panel.
   * @type {?JaSMIn.UI.Panel}
   */
  this.activePanel = null;



  // -------------------- Listeners -------------------- //
  /** @type {!Function} */
  this.visibilityListener = this.onVisibilityChanged.bind(this);
};



/**
 * Add the given panel to the group.
 *
 * @param  {!JaSMIn.UI.Panel} panel the panel to add
 */
JaSMIn.UI.PanelGroup.prototype.add = function(panel) {
  // Check if panel is already in the list of panels
  if (this.panels.indexOf(panel) !== -1) {
    return;
  }

  // Hide new panel
  JaSMIn.UI.setVisibility(panel.domElement, false);

  // Add new panel to group list
  this.panels.push(panel);

  // Add visibility change listener
  panel.onVisibilityChanged = this.visibilityListener;
};



/**
 * Check if this group has an active (visible) panel.
 *
 * @return {!boolean} true if an panel in this group is active (visible), false otherwise
 */
JaSMIn.UI.PanelGroup.prototype.hasActivePanel = function() {
  return this.activePanel !== null;
};



/**
 * Hide all (the currently active) panel.
 */
JaSMIn.UI.PanelGroup.prototype.hideAll = function() {
  if (this.activePanel !== null) {
    JaSMIn.UI.setVisibility(this.activePanel.domElement, false);
    this.activePanel = null;
  }
};



/**
 * Panel visibility change listener.
 *
 * @param  {!JaSMIn.UI.Panel} panel the panel which visibility changed
 */
JaSMIn.UI.PanelGroup.prototype.onVisibilityChanged = function(panel) {
  if (panel.isVisible()) {
    for (var i = 0; i < this.panels.length; i++) {
      if (this.panels[i] !== panel) {
        JaSMIn.UI.setVisibility(this.panels[i].domElement, false);
      }
    }

    this.activePanel = panel;
  } else if (this.activePanel === panel) {
    this.activePanel = null;
  }
};

