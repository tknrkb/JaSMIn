/**
 * The ResourceExplorer class definition.
 *
 * The ResourceExplorer provides browsing capabilities to various sources.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.ResourceExplorer');

goog.require('JaSMIn.MonitorModel');
goog.require('JaSMIn.UI');
goog.require('JaSMIn.UI.ArchiveExplorer');



/**
 * ResourceExplorer Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.TabPane}
 * @param {!JaSMIn.MonitorModel} model the monitor model instance
 */
JaSMIn.UI.ResourceExplorer = function(model) {
  goog.base(this, 'jsm-explorer');

  /**
   * The monitor model instance.
   * @type {!JaSMIn.MonitorModel}
   */
  this.model = model;

  /**
   * The archive explorer instance.
   * @type {!JaSMIn.UI.ArchiveExplorer}
   */
  this.archiveExplorer = new JaSMIn.UI.ArchiveExplorer(this.model.logPlayer);
  this.archiveExplorer.onGameLogSelected = function () {
    var mm = model;

    return function (url) {
      mm.loadGameLog(url);
    }
  }();
  // this.archiveExplorer.addLocation('http://archive.robocup.info/app/JaSMIn/archive.php', 'archive.robocup.info');
  // this.archiveExplorer.addLocation('http://localhost:8080/build/archive.php');
  // this.archiveExplorer.addLocation('archive.php', 'Archive');


  // Create Archive tab
  var headerPanel = new JaSMIn.UI.Panel();
  var label = JaSMIn.UI.createSpan('Archives');
  label.title = 'Browse Replay Archives';
  headerPanel.appendChild(label);

  this.addPanels(headerPanel, this.archiveExplorer);

  // Create Simulators tab
  // this.addElements(JaSMIn.UI.createSpan('Simulators'), JaSMIn.UI.createSpan('Simulator Browser'));

  // Create Streams tab
  // this.addElements(JaSMIn.UI.createSpan('Streams'), JaSMIn.UI.createSpan('Stream Browser'));


  /**
   * The file chooser input line, for selecting local files.
   * @type {!Element}
   */
  this.fileInput = JaSMIn.UI.createElement('input');
  this.fileInput.type = 'file';
  this.fileInput.accept = '.rpl3d, .rpl2d, .replay, .rcg, .json';
  this.fileInput.multiple = true;
  this.fileInput.onchange = function () {
    var mm = model;

    return function (evt) {
      var files = evt.target.files;

      if (files && files.length > 0) {
        mm.loadFiles(files);
      }
    };
  }();

  /**
   * The binded listener method for showing the file chooser dialog.
   * @type {!Function}
   */
  this.showFileChooserListener = this.showFileChooser.bind(this);


  /**
   * The open local resource button.
   * @type {!Element}
   */
  this.openResourceItem = JaSMIn.UI.createLI('open-resource');
  this.openResourceItem.onclick = this.showFileChooserListener;
  label = JaSMIn.UI.createSpan('Open');
  label.title = 'Open local resource...';
  this.openResourceItem.appendChild(label);
  this.openResourceItem.appendChild(this.fileInput);
  this.tabHeaderList.appendChild(this.openResourceItem);
};
goog.inherits(JaSMIn.UI.ResourceExplorer, JaSMIn.UI.TabPane);






/**
 * Show file chooser.
 *
 * @return {void}
 */
JaSMIn.UI.ResourceExplorer.prototype.showFileChooser = function() {
  this.fileInput.click();
};
