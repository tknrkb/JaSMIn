/**
 * The WelcomeOverlay class definition.
 *
 * The WelcomeOverlay abstracts
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.WelcomeOverlay');

goog.require('JaSMIn.UI');
goog.require('JaSMIn.UI.DnDHandler');



/**
 * WelcomeOverlay Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Panel}
 * @param {!JaSMIn.UI.DnDHandler} dndHandler the dnd-handler
 */
JaSMIn.UI.WelcomeOverlay = function(dndHandler) {
  goog.base(this, 'jsm-welcome-pane full-size');

  /**
   * The dnd handler instance.
   * @type {!JaSMIn.UI.DnDHandler}
   */
  this.dndHandler = dndHandler;


  /**
   * The Drag & Drop box for local files.
   * @type {!Element}
   */
  this.dndBox = JaSMIn.UI.createDiv('dnd-box');
  this.dndBox.innerHTML = '<span>Drag &amp; Drop Replays or SServer Logs to Play</span>';
  this.appendChild(this.dndBox);

  this.dndHandler.addListeners(this.dndBox);
};
goog.inherits(JaSMIn.UI.WelcomeOverlay, JaSMIn.UI.Panel);
