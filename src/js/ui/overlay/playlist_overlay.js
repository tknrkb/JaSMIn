/**
 * The PlaylistOverlay class definition.
 *
 * The PlaylistOverlay abstracts
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.UI.PlaylistOverlay');

goog.require('JaSMIn.UI.ToggleItem');



/**
 * PlaylistOverlay Constructor
 *
 * @constructor
 * @extends {JaSMIn.UI.Overlay}
 * @param {!JaSMIn.LogPlayer} logPlayer the log player model instance
 */
JaSMIn.UI.PlaylistOverlay = function(logPlayer) {
  goog.base(this, 'jsm-playlist');

  /**
   * The log player model instance.
   * @type {!JaSMIn.LogPlayer}
   */
  this.logPlayer = logPlayer;

  /**
   * The playlist instance.
   * @type {?JaSMIn.Playlist}
   */
  this.playlist = logPlayer.playlist;


  var titleBar = JaSMIn.UI.createDiv('title-bar');
  this.innerElement.appendChild(titleBar);

  /**
   * The playlist title label.
   * @type {!Element}
   */
  this.titleLbl = JaSMIn.UI.createSpan('My Playlist', 'title');
  titleBar.appendChild(this.titleLbl);


  var settingsBar = JaSMIn.UI.createDiv('settings-bar');
  this.innerElement.appendChild(settingsBar);

  var autoplayLbl = JaSMIn.UI.createSpan('Autoplay', 'label');
  autoplayLbl.title = 'Toggle Autoplay';
  autoplayLbl.onclick = this.toggleAutoplay.bind(this);
  settingsBar.appendChild(autoplayLbl);

  /**
   * The single choice form element.
   * @type {!Element}
   */
  this.autoplayForm = JaSMIn.UI.createSingleChoiceForm(['On', 'Off']);
  settingsBar.appendChild(this.autoplayForm);
  this.autoplayForm.onchange = this.handleAutoplayFormChange.bind(this);


  var contentBox = JaSMIn.UI.createDiv('content-box');
  this.innerElement.appendChild(contentBox);

  /**
   * The playlist entry list.
   * @type {!Element}
   */
  this.entryList = JaSMIn.UI.createUL('playlist');
  contentBox.appendChild(this.entryList);


  // -------------------- Listeners -------------------- //
  /** @type {!Function} */
  this.handlePlaylistChangeListener = this.handlePlaylistChange.bind(this);

  /** @type {!Function} */
  this.handlePlaylistUpdateListener = this.handlePlaylistUpdate.bind(this);
  /** @type {!Function} */
  this.handleAutoplayChangeListener = this.refreshAutoplay.bind(this);
  /** @type {!Function} */
  this.refreshSelectionsListener = this.refreshSelections.bind(this);
  /** @type {!Function} */
  this.refreshListingListener = this.refreshListing.bind(this);

  /** @type {!Function} */
  this.playEntryListener = this.playEntry.bind(this);


  // Add log player change listeners
  this.logPlayer.addEventListener(JaSMIn.LogPlayerEvents.PLAYLIST_CHANGE, this.handlePlaylistChangeListener);
  this.logPlayer.addEventListener(JaSMIn.LogPlayerEvents.GAME_LOG_CHANGE, this.refreshSelectionsListener);

  if (this.playlist !== null) {
    this.refreshListing();
    this.refreshAutoplay();

    this.playlist.addEventListener(JaSMIn.PlaylistEvents.UPDATE, this.handlePlaylistUpdateListener);
    this.playlist.addEventListener(JaSMIn.PlaylistEvents.CHANGE, this.refreshListingListener);
    this.playlist.addEventListener(JaSMIn.PlaylistEvents.ACTIVE_CHANGE, this.refreshSelectionsListener);
    this.playlist.addEventListener(JaSMIn.PlaylistEvents.AUTOPLAY_CHANGE, this.handleAutoplayChangeListener);
  }
};
goog.inherits(JaSMIn.UI.PlaylistOverlay, JaSMIn.UI.Overlay);






/**
 * Refresh the playlist items.
 *
 * @return {void}
 */
JaSMIn.UI.PlaylistOverlay.prototype.refreshListing = function() {
  var entryIndex = 0;
  var child;
  var entry;
  var newEntries = [];
  var playingIdx = this.logPlayer.playlistIndex;
  var selectedIdx = -1;

  if (this.playlist !== null) {
    selectedIdx = this.playlist.activeIndex;
    newEntries = this.playlist.entries;

    this.titleLbl.innerHTML = this.playlist.title;
  } else {
    this.titleLbl.innerHTML = 'n/a';
  }

  // Update all entry item nodes in entry list
  for (var i = 0; i < this.entryList.children.length; i++) {
    child = this.entryList.children[i];

    if (child.nodeName === 'LI') {
      entry = newEntries[entryIndex];

      // Refresh item entry
      this.refreshEntry(child, entry, entryIndex);
      this.refreshEntryClass(child, playingIdx, selectedIdx);

      entryIndex++;
    }
  }

  // Check if we need to add further item nodes
  while (entryIndex < newEntries.length) {
    entry = newEntries[entryIndex];
    child = JaSMIn.UI.createLI('entry');
    child.tabIndex = 0;

    this.refreshEntry(child, entry, entryIndex);
    this.refreshEntryClass(child, playingIdx, selectedIdx);

    child.addEventListener('click', this.playEntryListener, false);
    child.addEventListener('keydown', this.playEntryListener, false);

    this.entryList.appendChild(child);

    entryIndex++;
  }

  // TODO: Think about removing dead entries again, as switching from a very long to a rather short playlist may cause a lot of them...
};



/**
 * Refresh the css class of the given item entry.
 *
 * @param {!Element} item the list item element
 * @param {!number} playingIdx the index of the currently played game log
 * @param {!number} selectedIdx the index of the currently selected game log
 * @return {void}
 */
JaSMIn.UI.PlaylistOverlay.prototype.refreshEntryClass = function(item, playingIdx, selectedIdx) {
  var entryIdx = parseInt(item.dataset.entryIdx, 10);

  item.className = 'entry';
  if (entryIdx === playingIdx) {
    item.className += ' playing';
  } else if (entryIdx === selectedIdx) {
    item.className += ' selected';
  }

  if (item.dataset.valid !== 'true') {
    item.className += ' error';
  }
};



/**
 * Refresh the given item entry.
 *
 * @param {!Element} item the list item element
 * @param {!JaSMIn.GameLogEntry=} entry the game log entry instance
 * @param {!number=} index the entry index
 * @return {void}
 */
JaSMIn.UI.PlaylistOverlay.prototype.refreshEntry = function(item, entry, index) {
  if (index === undefined || entry === undefined) {
    // Clear item...
    item.dataset.entryIdx = -1;
    item.dataset.valid = 'false';
    item.title = '';
    item.innerHTML = '';

    // ... and hide it
    JaSMIn.UI.setVisibility(item, false);
  } else {
    // Update item data...
    item.dataset.entryIdx = index;
    item.dataset.valid = entry.errorMsg === null;
    item.title = entry.errorMsg !== null ? entry.errorMsg : '';

    // if (entry.info) {
    //   item.innerHTML = entry.info.leftTeamName + ' vs ' + entry.info.rightTeamName;
    // } else {
      item.innerHTML = entry.title;
    // }


    // ... and ensure it's visible
    JaSMIn.UI.setVisibility(item, true);
  }
};



/**
 * Refresh the autoplay toggle button.
 *
 * @return {void}
 */
JaSMIn.UI.PlaylistOverlay.prototype.refreshAutoplay = function() {
  if (this.playlist !== null) {
    this.autoplayForm.elements['userOptions'][this.playlist.autoplay ? 0 : 1].checked = true;
  }
};



/**
 * Refresh the selection status of the playlist items.
 *
 * @return {void}
 */
JaSMIn.UI.PlaylistOverlay.prototype.refreshSelections = function() {
  if (this.playlist !== null) {
    var playingIdx = this.logPlayer.playlistIndex;
    var selectedIdx = this.playlist.activeIndex;
    var child;

    // Update all entry item nodes in entry list
    for (var i = 0; i < this.entryList.children.length; i++) {
      child = this.entryList.children[i];

      if (child.nodeName === 'LI') {
        this.refreshEntryClass(child, playingIdx, selectedIdx);
      }
    }
  }
};



/**
 * Action handler for playing en entry of the playlist.
 *
 * @param {!Event} evt the click event
 * @return {void}
 */
JaSMIn.UI.PlaylistOverlay.prototype.playEntry = function(evt) {
  if (!JaSMIn.UI.isButtonAction(evt)) {
    return;
  }

  if (this.playlist !== null) {
    var idx = evt.target.dataset.entryIdx;

    if (idx !== undefined && evt.target.dataset.valid === 'true') {
      this.playlist.setActiveIndex(parseInt(idx, 10));
    }
  }
};



/**
 * Change listener callback function for the autoplay single choice form element.
 *
 * @return {void}
 */
JaSMIn.UI.PlaylistOverlay.prototype.handleAutoplayFormChange = function() {
  if (this.playlist === null) {
    return;
  }

  if (this.autoplayForm.elements['userOptions'][0].checked) {
    // Autoplay is on
    this.playlist.setAutoplay(true);
  } else {
    // Autoplay is off
    this.playlist.setAutoplay(false);
  }
};



/**
 * Toggle autoplay of the playlist.
 *
 * @return {void}
 */
JaSMIn.UI.PlaylistOverlay.prototype.toggleAutoplay = function() {
  if (this.playlist !== null) {
    this.playlist.setAutoplay(!this.playlist.autoplay);
  }
};



/**
 * Handle playlist updated event.
 *
 * @param {!Object} evt the event object
 * @return {void}
 */
JaSMIn.UI.PlaylistOverlay.prototype.handlePlaylistUpdate = function(evt) {
  var entryIdx = evt.index;
  var entry = evt.entry;
  var child;

  // Update all entry item nodes in entry list
  for (var i = 0; i < this.entryList.children.length; i++) {
    child = this.entryList.children[i];

    if (child.nodeName === 'LI') {
      if (entryIdx === parseInt(child.dataset.entryIdx, 10)) {
        if (entry.errorMsg !== null) {
          child.dataset.valid = false;
          child.title = entry.errorMsg;
        } else {
          child.dataset.valid = true;
          child.title = '';
        }

        this.refreshEntryClass(child, this.logPlayer.playlistIndex, this.playlist.activeIndex);
        break;
      }
    }
  }
};



/**
 * Handle playlist change.
 *
 * @param {!Object} evt the change event
 * @return {void}
 */
JaSMIn.UI.PlaylistOverlay.prototype.handlePlaylistChange = function(evt) {
  if (this.playlist !== null) {
    this.playlist.removeEventListener(JaSMIn.PlaylistEvents.UPDATE, this.handlePlaylistUpdateListener);
    this.playlist.removeEventListener(JaSMIn.PlaylistEvents.CHANGE, this.refreshListingListener);
    this.playlist.removeEventListener(JaSMIn.PlaylistEvents.ACTIVE_CHANGE, this.refreshSelectionsListener);
    this.playlist.removeEventListener(JaSMIn.PlaylistEvents.AUTOPLAY_CHANGE, this.handleAutoplayChangeListener);
  }

  this.playlist = this.logPlayer.playlist;
  this.refreshListing();
  this.refreshAutoplay();

  if (this.playlist !== null) {
    this.playlist.addEventListener(JaSMIn.PlaylistEvents.UPDATE, this.handlePlaylistUpdateListener);
    this.playlist.addEventListener(JaSMIn.PlaylistEvents.CHANGE, this.refreshListingListener);
    this.playlist.addEventListener(JaSMIn.PlaylistEvents.ACTIVE_CHANGE, this.refreshSelectionsListener);
    this.playlist.addEventListener(JaSMIn.PlaylistEvents.AUTOPLAY_CHANGE, this.handleAutoplayChangeListener);
  } else {
    // Hide playlist overlay if no playlist available
    this.setVisible(false);
  }
};
