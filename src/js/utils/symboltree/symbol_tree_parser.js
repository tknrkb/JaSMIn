/**
 * The SymbolTreeParser class definition.
 *
 * The SymbolTreeParser allows parsing a symbol tree string into a tree representation.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.SymbolTreeParser');

goog.require('JaSMIn');
goog.require('JaSMIn.SymbolNode');



/**
 * SymbolTreeParser definition.
 */
JaSMIn.SymbolTreeParser = { };


/**
 * Parse the given string into a symbol tree representation.
 *
 * @param {!string} input the string to parse
 * @return {!JaSMIn.SymbolNode} [description]
 */
JaSMIn.SymbolTreeParser.parse = function(input) {
  var rootNode = new JaSMIn.SymbolNode();

  if (input.charAt(0) !== '(' || input.charAt(input.length - 1) !== ')') {
    throw 'Input not embedded in braces: ' + input;
  }


  var pos = JaSMIn.SymbolTreeParser.parseNode(input, 1, rootNode);
  if (pos !== input.length) {
    throw 'Multiple root nodes in input: ' + input;
  }

  return rootNode.children[0];
};


/**
 * Parse the given string into a symbol tree representation.
 *
 * @param {!string} input the string to parse
 * @param {!number} startIdx the index to start
 * @param {!JaSMIn.SymbolNode} parentNode the parent node
 * @return {!number} the index after parsing the node
 */
JaSMIn.SymbolTreeParser.parseNode = function(input, startIdx, parentNode) {
  // Create a new node
  var newNode = new JaSMIn.SymbolNode();
  parentNode.children.push(newNode);

  var idx = startIdx;

  while (idx < input.length) {
    if (input.charAt(idx) === '(') {
      // Found a new subnode
      if (idx > startIdx) {
        // Add value to node
        newNode.values.push(input.slice(startIdx, idx));
      }
      startIdx = idx = JaSMIn.SymbolTreeParser.parseNode(input, idx + 1, newNode);
    } else if (input.charAt(idx) === ')') {
      // Found node terminator for this node
      if (idx > startIdx) {
        // Add value to node
        newNode.values.push(input.slice(startIdx, idx));
      }
      return idx + 1;
    } else if (input.charAt(idx) === ' ') {
      // Found new value terminator
      if (idx > startIdx) {
        // Add value to node
        newNode.values.push(input.slice(startIdx, idx));
      }
      idx++;
      startIdx = idx;
    } else {
      idx++;
    }
  }

  throw 'Invalid tree structure in input: ' + input;
};
