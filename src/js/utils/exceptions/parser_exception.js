/**
 * The ParserException class definition.
 *
 * The ParserException provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.ParserException');

goog.require('JaSMIn');



/**
 * ParserException Constructor
 *
 * @constructor
 * @param {!string=} msg the exception message
 */
JaSMIn.ParserException = function(msg) {

  /**
   * The name of the exception.
   * @type {!string}
   */
  this.name = 'ParserException';

  /**
   * The exception message.
   * @type {!string}
   */
  this.message = msg !== undefined ? msg : this.name;
};



/**
 * @override
 */
JaSMIn.ParserException.prototype.toString = function() {
  return this.message;
};
