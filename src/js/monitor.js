goog.provide('JaSMIn.Monitor');

goog.require('JaSMIn');
goog.require('JaSMIn.MonitorModel');
goog.require('JaSMIn.MonitorParameter');
goog.require('JaSMIn.UI.MonitorUI');




/**
 * External Monitor API.
 *
 * Embedded vs. Standalone:
 * The player can run in embedded or standalone mode. While the standalone
 * version features full functionality integrated in the player itself, the
 * embedded version only provides the core monitor/player component and
 * expects to be commanded from outside the player component.
 * By default, the player runs in standalone mode. To enable the embedded mode,
 * provide the following parameter to the player:
 * params['embedded'] = true
 *
 * Autoplay:
 * The player can check the address-line parameters for a replay path. If
 * autoplay is enabled, the player will look for a replay file path in the
 * address-line and try to load and play it straight away.
 * params['autoplay'] = true
 *
 *
 *
 * Parameter Object:
 * params['embedded'] = <boolean>
 * params['archives'] = undefined | [{url:<string>, name:<string>}, ...]
 *
 * @export
 * @constructor
 * @param {Element=} containerElement the parent element of the monitor
 * @param {Object=} params the parameter object
 */
JaSMIn.Monitor = function(containerElement, params) {
  // Fetch a valid root container
  var container = document.body;
  if (containerElement) {
    container = containerElement;

    // Clear player container (to remove placeholders)
    container.innerHTML = '';
  }

  // Create parameter wrapper object
  var monitorParams = new JaSMIn.MonitorParameter(params);


  /**
   * The monitor model.
   * @type {!JaSMIn.MonitorModel}
   */
  this.model = new JaSMIn.MonitorModel(monitorParams.isEmbedded());

  /**
   * The monitor user interface.
   * @type {!JaSMIn.UI.MonitorUI}
   */
  this.ui = new JaSMIn.UI.MonitorUI(this.model, container);


  try {
    this.applyParams(monitorParams);
  } catch (ex) {
    console.log('Error while applying monitor parameters!');
  }
};



/**
 * Apply the given monitor parameter.
 *
 * @param {!JaSMIn.MonitorParameter} params the monitor params
 * @return {void}
 */
JaSMIn.Monitor.prototype.applyParams = function(params) {
  // Add Archives
  var archives = params.getArchives();
  for (var i = 0; i < archives.length; i++) {
    if (archives[i].url && archives[i].name) {
      this.ui.resourceExplorer.archiveExplorer.addLocation(archives[i].url, archives[i].name);
    }
  }


  // Check for resource path parameters
  var url = params.getGameLogURL();
  if (url) {
    // Found game log url
    this.loadGameLog(url);
    this.ui.hideExplorer();
  } else {
    // Check for playlist path parameter
    url = params.getPlaylistURL();

    if (url) {
      this.loadPlaylist(url);
      this.ui.hideExplorer();
    }
  }
};



/**
 * Load and play a game log file.
 *
 * @export
 * @param {!string} url the game log file url
 * @return {void}
 */
JaSMIn.Monitor.prototype.loadGameLog = function(url) {
  this.model.loadGameLog(url);
};



/**
 * Load a playlist.
 *
 * @export
 * @param {!string} url the playlist url
 * @return {void}
 */
JaSMIn.Monitor.prototype.loadPlaylist = function(url) {
  this.model.loadPlaylist(url);
};



/**
 * Try to load the given files.
 *
 * @param {!Array<!File>} files a list of local files to load/open
 */
JaSMIn.Monitor.prototype.loadFiles = function(files) {
  this.model.loadFiles(files);
};



/**
 * Connect to the given streaming server and play the replay stream.
 *
 * @export
 * @param {!string} url the replay streaming server url
 * @return {void}
 */
JaSMIn.Monitor.prototype.connectStream = function(url) {
  this.model.connectStream(url);
};



/**
 * Connect to a simulation server.
 *
 * @export
 * @param {!string} url the simulation server web-socket url.
 * @return {void}
 */
JaSMIn.Monitor.prototype.connectSimulator = function(url) {
  this.model.connectSimulator(url);
};



/**
 * Trigger play/pause command.
 *
 * @export
 * @return {void}
 */
JaSMIn.Monitor.prototype.playPause = function() {
  this.model.logPlayer.playPause();
};



/**
 * Trigger stop command.
 *
 * @export
 * @return {void}
 */
JaSMIn.Monitor.prototype.stop = function() {

};



/**
 * Trigger step command.
 *
 * @export
 * @param {!boolean=} backwards fowrwards/backwards direction indicator (default: forward)
 * @return {void}
 */
JaSMIn.Monitor.prototype.step = function(backwards) {
  this.model.logPlayer.step(backwards);
};



/**
 * Trigger jump command.
 *
 * @export
 * @param {!number} stateIdx the state index to jump to. Negative values are interpreted as: (statesArray.length + stateIdx)
 * @return {void}
 */
JaSMIn.Monitor.prototype.jump = function(stateIdx) {
  this.model.logPlayer.jump(stateIdx);
};



/**
 * Trigger jump goal command.
 *
 * @export
 * @param {!boolean=} previous next/previous indicator (default: next)
 * @return {void}
 */
JaSMIn.Monitor.prototype.jumpGoal = function(previous) {
  this.model.logPlayer.jumpGoal(previous);
};
