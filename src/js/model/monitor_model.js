/**
 * The MonitorModel definition.
 *
 * The MonitorModel provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.MonitorModel');

goog.require('JaSMIn');
goog.require('JaSMIn.LogPlayer');
goog.require('JaSMIn.MonitorSettings');
goog.require('JaSMIn.Playlist');
goog.require('JaSMIn.World');






/**
 * The monitor model event type enum.
 * @enum {!string}
 */
JaSMIn.MonitorModelEvents = {
  STATE_CHANGE: 'state-change',
};



/**
 * The monitor model state/mode enum.
 * @enum {!string}
 */
JaSMIn.MonitorStates = {
  INIT: 'init',
  REPLAY: 'replay',
  STREAM: 'stream',
  LIVE: 'live'
};






/**
 * MonitorModel Constructor
 *
 * @constructor
 * @implements {JaSMIn.IPublisher}
 * @implements {JaSMIn.IEventDispatcher}
 * @param {!boolean} embedded indicator if the monitor is in embedded mode
 */
JaSMIn.MonitorModel = function(embedded) {

  /**
   * Indicator if the monitor is started in embedded mode.
   * @type {!boolean}
   */
  this.embedded = embedded;

  /**
   * The current state of the monitor.
   * @type {!JaSMIn.MonitorStates}
   */
  this.state = JaSMIn.MonitorStates.INIT;

  /**
   * The various monitor settings.
   * @type {!JaSMIn.MonitorSettings}
   */
  this.settings = new JaSMIn.MonitorSettings();

  /**
   * The GL world instance.
   * @type {!JaSMIn.World}
   */
  this.world = new JaSMIn.World();
  this.world.setShadowsEnabled(this.settings.monitorConfig.shadowsEnabled);

  /**
   * The game log player instance.
   * @type {!JaSMIn.LogPlayer}
   */
  this.logPlayer = new JaSMIn.LogPlayer(this.world, this.settings.monitorConfig);



  /** @type {!Function} */
  this.handleLogPlayerChangeListener = this.handleLogPlayerChange.bind(this);

  // Add log player event listeners
  this.logPlayer.addEventListener(JaSMIn.LogPlayerEvents.GAME_LOG_CHANGE, this.handleLogPlayerChangeListener);
  this.logPlayer.addEventListener(JaSMIn.LogPlayerEvents.PLAYLIST_CHANGE, this.handleLogPlayerChangeListener);
};






/**
 * Set the state of the monitor model.
 *
 * @param {!JaSMIn.MonitorStates} newState the new monitor model state
 */
JaSMIn.MonitorModel.prototype.setState = function(newState) {
  if (this.state === newState) {
    // Already in the "new" state, thus nothing to do
    return;
  }

  var oldState = this.state;
  this.state = newState;

  // Publish state change event
  this.dispatchEvent({
      type: JaSMIn.MonitorModelEvents.STATE_CHANGE,
      oldState: oldState,
      newState: newState
    });
};



/**
 * Try to load the game log at the specified url.
 *
 * @param {!string} url the game log url
 */
JaSMIn.MonitorModel.prototype.loadGameLog = function(url) {
  this.logPlayer.loadGameLog(url);
};



/**
 * Try to load the playlist at the specified url.
 *
 * @param {!string} url the playlist url
 */
JaSMIn.MonitorModel.prototype.loadPlaylist = function(url) {
  this.logPlayer.loadPlaylist(url);
};



/**
 * Try to load the given files.
 *
 * @param {!Array<!File>} files a list of local files to load/open
 */
JaSMIn.MonitorModel.prototype.loadFiles = function(files) {
  // Check for game log file(s) (.replay, .rpl2d, .rpl3d, .rcg)
  //  -> check for single game log file
  //  -> check for multiple game log files (playlist)
  //
  // Check for json file (.json)
  //  -> check for archive definition
  //  -> check for playlist definition
  var gameLogFiles = JaSMIn.filterFiles(files, ['.replay', '.rpl2d', '.rpl3d', '.rcg']);
  var jsonFiles = JaSMIn.filterFiles(files, ['.json']);

  if (gameLogFiles.length === 1) {
    // Load single game log file
    this.logPlayer.loadGameLogFile(gameLogFiles[0]);
  } else if(gameLogFiles.length > 1) {
    // Create a game log playlist
    var playlist = new JaSMIn.Playlist('Local Playlist');
    playlist.addFiles(gameLogFiles);

    this.logPlayer.setPlaylist(playlist);
  } else if (jsonFiles.length > 0) {
    for (var i = 0; i < jsonFiles.length; i++) {
      // Process json-files individually

    }
  } else if (files.length > 0) {
    alert('Unsupported file type(s)!');
  }
};



/**
 * Connect to the given streaming server.
 *
 * @param {!string} url the replay streaming server url
 * @return {void}
 */
JaSMIn.MonitorModel.prototype.connectStream = function(url) {
  throw 'MonitorModel::connectStream(): Not implemented yet!';
};



/**
 * Connect to a simulation server.
 *
 * @param {!string} url the simulation server web-socket url.
 * @return {void}
 */
JaSMIn.MonitorModel.prototype.connectSimulator = function(url) {
  throw 'MonitorModel::connectSimulator(): Not implemented yet!';
};



/**
 * LogPlayer->"game-log-change"|"playlist-change" event listener.
 * This event listener is triggered when the game log or the playlist within the player has changed.
 *
 * @param {!Object} evt the event object
 * @return {void}
 */
JaSMIn.MonitorModel.prototype.handleLogPlayerChange = function(evt) {
  // Make sure the monitor is in replay mode
  this.setState(JaSMIn.MonitorStates.REPLAY);
};






// ============================== EVENT DISPATCHER FUNCTIONS ==============================
/** @override */
JaSMIn.MonitorModel.prototype.addEventListener = JaSMIn.EventDispatcher.prototype.addEventListener;
/** @override */
JaSMIn.MonitorModel.prototype.removeEventListener = JaSMIn.EventDispatcher.prototype.removeEventListener;
/** @override */
JaSMIn.MonitorModel.prototype.dispatchEvent = JaSMIn.EventDispatcher.prototype.dispatchEvent;
// ============================== EVENT DISPATCHER FUNCTIONS ==============================
