/**
 * The Agent class definition.
 *
 * The Agent provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.Agent');

goog.require('JaSMIn.AgentDescription');
goog.require('JaSMIn.RobotModel');



/**
 * Agent Constructor
 *
 * @constructor
 * @extends {JaSMIn.MovableObject}
 * @param {!JaSMIn.AgentDescription} description the agent description
 */
JaSMIn.Agent = function(description) {
  goog.base(this, 'agent_' + description.getSideLetter() + description.playerNo);

  /**
   * The agent description.
   * @type {!JaSMIn.AgentDescription}
   */
  this.description = description;

  /**
   * The list of robot models
   * @type {!Array<!JaSMIn.RobotModel>}
   */
  this.models = [];
};
goog.inherits(JaSMIn.Agent, JaSMIn.MovableObject);



/**
 * Update this agent's state.
 *
 * @param  {!JaSMIn.AgentState=} state the current state
 * @param  {!JaSMIn.AgentState=} nextState the next state
 * @param  {!number=} t the interpolation step
 * @return {void}
 */
JaSMIn.Agent.prototype.update = function(state, nextState, t) {
  if (state === undefined || state.isValid() === false) {
    // Invalid data, thus kill agent
    this.objGroup.visible = false;
    return;
  } else if (this.objGroup.visible === false) {
    // Valid data, thus revive agent
    this.objGroup.visible = true;
  }

  // Update position and rotation of agent root object group
  this.updateBodyPose(state, nextState, t);

  // Activate agent model to current state
  this.setActiveModel(state.modelIdx);

  if (this.models[state.modelIdx] !== undefined) {
    var nextAngles = nextState !== undefined ? nextState.jointAngles : undefined;
    var nextData = nextState !== undefined ? nextState.data : undefined;

    this.models[state.modelIdx].update(state.jointAngles, state.data, nextAngles, nextData, t);
  }
};


/**
 * Set the active robot model to the current state
 * @param {!number} modelIdx the index of the model
 */
JaSMIn.Agent.prototype.setActiveModel = function(modelIdx) {
  if (this.models[modelIdx] !== undefined && !this.models[modelIdx].isActive()) {
    var i = this.models.length;

    while (i--) {
      this.models[i].setActive(i === modelIdx);
    }
  }
};



/**
 * Set agent team color
 *
 * @param {THREE.Color} color the new team color
 */
JaSMIn.Agent.prototype.setTeamColor = function(color) {
  var i = this.models.length;

  while (i--) {
    this.models[i].setTeamColor(color);
  }
};
