goog.provide('JaSMIn.RobotModelFactory');

goog.require('JaSMIn.ParameterMap');
goog.require('JaSMIn.RobotModel');



/**
 * RobotModelFactory Interface.
 *
 * @interface
 */
JaSMIn.RobotModelFactory = function() {};


/**
 * Create a robot model to the given player type.
 *
 * @param  {!JaSMIn.ParameterMap} playerType the player type
 * @param  {!JaSMIn.TeamSide} side the team side
 * @param  {!number} playerNo the player number
 * @param  {!JaSMIn.ParameterMap} environmentParams the environment paraméter
 * @param  {!JaSMIn.ParameterMap} playerParams the player paraméter
 * @return {?JaSMIn.RobotModel} a new robot model
 */
JaSMIn.RobotModelFactory.prototype.createModel = function(playerType, side, playerNo, environmentParams, playerParams) {};


/**
 * Dispose all resources allocated within this factory.
 *
 * @return {void}
 */
JaSMIn.RobotModelFactory.prototype.dispose = function() {};
