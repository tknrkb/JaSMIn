/**
 * The Ball class definition.
 *
 * The Ball provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.Ball');



/**
 * Ball Constructor
 *
 * @constructor
 * @extends {JaSMIn.MovableObject}
 * @param {!number=} radius the ball radius
 */
JaSMIn.Ball = function(radius) {
  goog.base(this, 'ball');

  this.radius = radius !== undefined ? radius : 0.2;

  this.objGroup.scale.setScalar(this.radius);
};
goog.inherits(JaSMIn.Ball, JaSMIn.MovableObject);



/**
 * Set the ball radius.
 *
 * @param  {!number} radius the new ball radius
 * @return {void}
 */
JaSMIn.Ball.prototype.setRadius = function(radius) {
  if (this.radius !== radius) {
    this.radius = radius;
    this.objGroup.scale.setScalar(this.radius);
  }
};



/**
 * Update movable object
 *
 * @param  {!JaSMIn.ObjectState} state the current object state
 * @param  {!JaSMIn.ObjectState=} nextState the next object state
 * @param  {!number=} t the interpolated time between the current and next state
 * @return {void}
 */
JaSMIn.Ball.prototype.update = function(state, nextState, t) {
  this.updateBodyPose(state, nextState, t);
};
