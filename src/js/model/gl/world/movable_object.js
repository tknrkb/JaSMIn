/**
 * The MovableObject class definition.
 *
 * The MovableObject provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.MovableObject');

goog.require('JaSMIn.ObjectState');
goog.require('JaSMIn.SceneUtils');



/**
 * MovableObject Constructor
 *
 * @constructor
 * @param {string} name the name of the movable object
 */
JaSMIn.MovableObject = function(name) {
  /**
   * The movable object group.
   * @type {!THREE.Object3D}
   */
  this.objGroup = new THREE.Object3D();
  this.objGroup.name = name;

  /**
   * The movable ground 2D object group.
   * @type {!THREE.Object3D}
   */
  this.objTwoDGroup = new THREE.Object3D();
  this.objTwoDGroup.name = name + '_2D';

  /**
   * The object representing that this object is selected.
   * @type {!THREE.Mesh}
   */
  this.selectionObj = JaSMIn.SceneUtils.createSelectionMesh(0.15, 0.02);
  this.objTwoDGroup.add(this.selectionObj);
};



/**
 * Highlight or normalize object representation.
 *
 * @param {!boolean} selected true for selected, false for deseleced
 */
JaSMIn.MovableObject.prototype.setSelected = function(selected) {
  this.selectionObj.visible = selected;
};


/**
 * [updateBodyPose description]
 *
 * @param  {!JaSMIn.ObjectState} state the current object state
 * @param  {!JaSMIn.ObjectState=} nextState the next object state
 * @param  {!number=} t the interpolated time between the current and next state
 * @return {void}
 */
JaSMIn.MovableObject.prototype.updateBodyPose = function(state, nextState, t) {
  // Update position and orientation of this root object group
  if (nextState !== undefined && nextState.isValid() && t > 0) {
    if (t >= 1) {
      this.objGroup.position.copy(nextState.position);
      this.objGroup.quaternion.copy(nextState.quaternion);
    } else {
      this.objGroup.position.lerpVectors(state.position, nextState.position, t);
      THREE.Quaternion.slerp(state.quaternion, nextState.quaternion, this.objGroup.quaternion, t);
    }
  } else {
    this.objGroup.position.copy(state.position);
    this.objGroup.quaternion.copy(state.quaternion);
  }

  // Copy 2D position and orientation to 2D object group
  this.objTwoDGroup.position.x = this.objGroup.position.x;
  this.objTwoDGroup.position.z = this.objGroup.position.z;
  // DOTO: extract heading angle

  // Trigger update of object matrices
  this.objGroup.updateMatrix();
  this.objTwoDGroup.updateMatrix();
};
