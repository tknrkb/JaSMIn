/**
 * The SServerLog class definition.
 *
 * The SServerLog is the central class holding a soccer-server 2D game log file.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.SServerLog');

goog.require('JaSMIn');
goog.require('JaSMIn.PartialWorldState');





/**
 * SServerLog Constructor
 * Create a new sserver game log file.
 *
 * @constructor
 * @extends {JaSMIn.GameLog}
 * @param {!number} version the ulg log version
 */
JaSMIn.SServerLog = function(version) {
  goog.base(this, JaSMIn.GameType.TWOD);

  /**
   * The ulg log version.
   * @type {!number}
   */
  this.version = version;
};
goog.inherits(JaSMIn.SServerLog, JaSMIn.GameLog);
