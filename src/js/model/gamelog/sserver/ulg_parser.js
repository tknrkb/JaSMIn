/**
 * The ULGParser class definition.
 *
 * The ULGParser provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.ULGParser');

goog.require('JaSMIn');
goog.require('JaSMIn.AgentDescription');
goog.require('JaSMIn.AgentState');
goog.require('JaSMIn.DataIterator');
goog.require('JaSMIn.LogParserStorage');
goog.require('JaSMIn.ObjectState');
goog.require('JaSMIn.ParserException');
goog.require('JaSMIn.SServerLog');
goog.require('JaSMIn.SymbolNode');
goog.require('JaSMIn.SymbolTreeParser');
goog.require('JaSMIn.TeamDescription');
goog.require('JaSMIn.WorldState');





/**
 * ULGParser Constructor
 * Create a new ULG parser instance.
 *
 * @constructor
 * @implements {JaSMIn.GameLogParser}
 */
JaSMIn.ULGParser = function() {

  /**
   * The ulg log data iterator.
   * @type {?JaSMIn.DataIterator}
   */
  this.iterator = null;

  /**
   * The sserver log.
   * @type {?JaSMIn.SServerLog}
   */
  this.sserverLog = null;

  /**
   * The storage instance used during parsing.
   * @type {?JaSMIn.LogParserStorage}
   */
  this.storage = null;

  // console.log('New USG parser instance created!');
};



/**
 * @override
 */
JaSMIn.ULGParser.prototype.parse = function(data, extent) {
  if (this.iterator === null || this.sserverLog === null || this.storage === null) {
    // Start parsing
    this.iterator = new JaSMIn.DataIterator(data, extent !== undefined ? extent : JaSMIn.DataExtent.COMPLETE);

    // ==================== Check ULG Header ====================
    var line = this.iterator.next();
    if (line === null || (line.charAt(0) !== 'U' && line.charAt(1) !== 'L' && line.charAt(2) !== 'G')) {
      throw new JaSMIn.ParserException('Failed parsing ULG log file - no ULG header found!');
    }
    var ulgVersion = parseInt(line.slice(3), 10);
    this.sserverLog = new JaSMIn.SServerLog(ulgVersion);
    this.storage = new JaSMIn.LogParserStorage();
    this.storage.partialState = new JaSMIn.PartialWorldState(0, 0.1, 0);
    this.storage.maxStates = 100;

    // Start parsing the ulg log body
    this.iterator.next();
    JaSMIn.ULGParser.parseULGBody(this.iterator, this.sserverLog, this.storage);
    return true;
  } else if (this.sserverLog !== null) {
    // Progress parsing
    if (this.iterator.update(data, extent !== undefined ? extent : JaSMIn.DataExtent.COMPLETE)) {
      // console.log('Restarting ULG parser...');
      JaSMIn.ULGParser.parseULGBody(this.iterator, this.sserverLog, this.storage);
    }
  }

  return false;
};



/**
 * Retrieve the currently parsed game log.
 *
 * @return {?JaSMIn.GameLog} the (maybe partially) parsed game log
 */
JaSMIn.ULGParser.prototype.getGameLog = function() {
  return this.sserverLog;
};



/**
 * Dispose all resources referenced in this parser instance.
 *
 * @param {!boolean=} keepIteratorAlive indicator if iterator should not be disposed
 * @return {void}
 */
JaSMIn.ULGParser.prototype.dispose = function(keepIteratorAlive) {
  // console.log('Dispose USG parser instance (keep iterator: ' + keepIteratorAlive + ')');

  if (this.iterator !== null && !keepIteratorAlive) {
    this.iterator.dispose();
  }

  this.iterator = null;
  this.sserverLog = null;
  this.storage = null;
};









// ============================================================================
// ============================== STATIC MEMBERS ==============================
// ============================================================================

/**
 * [parseULGBody description]
 *
 * @param  {!JaSMIn.DataIterator} iterator the ulg log data iterator
 * @param  {!JaSMIn.SServerLog} sserverLog the sserver log to store the parsed states
 * @param  {!JaSMIn.LogParserStorage} storage the parser storage instance
 * @return {void}
 */
JaSMIn.ULGParser.parseULGBody = function(iterator, sserverLog, storage) {
  // console.log('Parsing ulg log body...');

  var line = iterator.line;
  if (line === null) {
    // Try to restart the iterator
    line = iterator.next();
  }

  var newStatesCnt = 0;

  // Parse
  while (line !== null && newStatesCnt < storage.maxStates) {
    try {
      if (line.slice(0, 14) === '(server_param ') {
        JaSMIn.ULGParser.parseServerParamLine(line, sserverLog, storage);
      } else if (line.slice(0, 14) === '(player_param ') {
        JaSMIn.ULGParser.parsePlayerParamLine(line, sserverLog);
      } else if (line.slice(0, 13) === '(player_type ') {
        JaSMIn.ULGParser.parsePlayerTypeLine(line, sserverLog);
      } else if (line.slice(0, 6) === '(team ') {
        JaSMIn.ULGParser.parseTeamLine(line, sserverLog, storage);
      } else if (line.slice(0, 10) === '(playmode ') {
        JaSMIn.ULGParser.parsePlaymodeLine(line, sserverLog, storage);
      } else if (line.slice(0, 5) === '(msg ') {
        JaSMIn.ULGParser.parseMessageLine(line, sserverLog);
      } else if (line.slice(0, 6) === '(draw ') {
        JaSMIn.ULGParser.parseDrawLine(line, sserverLog);
      } else if (line.slice(0, 6) === '(show ') {
        JaSMIn.ULGParser.parseShowLine(line, sserverLog, storage);
        newStatesCnt++;
      } else {
        console.log('Unknown ulg log line: ' + line);
      }
    } catch (ex) {
    }

    line = iterator.next();
  }

  // Refresh sserver log
  if (newStatesCnt > 0) {
    sserverLog.onStatesUpdated();
  }

  // Start parsing job, parsing 100 show lines per run
  if (line !== null) {
    setTimeout(JaSMIn.ULGParser.parseULGBody, 1, iterator, sserverLog, storage);
  } else if (iterator.extent === JaSMIn.DataExtent.COMPLETE) {
    iterator.dispose();

    if (storage.hasPartialState()) {
      // Push final state
      storage.partialState.appendTo(sserverLog.states);
    }

    sserverLog.finalize();
  }
};



/**
 * @param  {!string} line the playmode line
 * @param  {!JaSMIn.SServerLog} sserverLog the ssserver log
 * @param  {!JaSMIn.LogParserStorage} storage the parser storage instance
 * @return {void}
 */
JaSMIn.ULGParser.parsePlaymodeLine = function(line, sserverLog, storage) {
  // (playmode <gameTime> <playmode>)
  var rootNode = JaSMIn.SymbolTreeParser.parse(line);

  if (rootNode.values.length > 2) {
    var gameTime = parseInt(rootNode.values[1], 10) / 10;

    // Add partial state if valid
    storage.partialState.appendTo(sserverLog.states);

    // Update partial word state
    storage.partialState.setGameTime(gameTime);
    storage.partialState.setPlaymode(rootNode.values[2]);
  } else {
    console.log('Invalid playmode line: ' + line);
  }
};



/**
 * @param  {!string} line the team line
 * @param  {!JaSMIn.SServerLog} sserverLog the ssserver log
 * @param  {!JaSMIn.LogParserStorage} storage the parser storage instance
 * @return {void}
 */
JaSMIn.ULGParser.parseTeamLine = function(line, sserverLog, storage) {
  // (team <time> <left-team-name> <right-team-name> <goals-left> <goals-right> [<pen-score-left> <pen-miss-left> <pen-score-right> <pen-miss-right>])
  var rootNode = JaSMIn.SymbolTreeParser.parse(line);

  if (rootNode.values.length > 5) {
    var gameTime = parseInt(rootNode.values[1], 10) / 10;
    var goalsLeft = parseInt(rootNode.values[4], 10);
    var goalsRight = parseInt(rootNode.values[5], 10);
    var penScoreLeft = 0;
    var penMissLeft = 0;
    var penScoreRight = 0;
    var penMissRight = 0;

    if (rootNode.values.length > 9) {
      penScoreLeft = parseInt(rootNode.values[6], 10);
      penMissLeft = parseInt(rootNode.values[7], 10);
      penScoreRight = parseInt(rootNode.values[8], 10);
      penMissRight = parseInt(rootNode.values[9], 10);
    }

    // Update left team name
    if (sserverLog.leftTeam.setName(JaSMIn.copyString(rootNode.values[2])) |
        sserverLog.rightTeam.setName(JaSMIn.copyString(rootNode.values[3]))) {
      sserverLog.onTeamsUpdated();
    }

    // Add partial state if valid
    storage.partialState.appendTo(sserverLog.states);

    // Update partial word state
    storage.partialState.setGameTime(gameTime);
    storage.partialState.setScore(goalsLeft, goalsRight, penScoreLeft, penMissLeft, penScoreRight, penMissRight);
  } else {
    console.log('Invalid team line: ' + line);
  }
};



/**
 * @param  {!string} line the show line
 * @param  {!JaSMIn.SServerLog} sserverLog the ssserver log
 * @param  {!JaSMIn.LogParserStorage} storage the parser storage instance
 * @return {void}
 */
JaSMIn.ULGParser.parseShowLine = function(line, sserverLog, storage) {
  // (show <time>
  //    [(pm <playmode-no>)]
  //    [(tm <left-team-name> <right-team-name> <goals-left> <goals-right> [<pen-score-left> <pen-miss-left> <pen-score-right> <pen-miss-right>])]
  //    ((b) <x> <y> <vx> <vy>)
  //    [((<side> <unum>) <player_type> <flags> <x> <y> <vx> <vy> <body> <neck> [<point-x> <point-y>]
  //        (v <view-quality> <view-width>)
  //        (s <stamina> <effort> <recovery> [<capacity>])
  //        [(f <side> <unum>)]
  //        (c <kick> <dash> <turn> <catch> <move> <tneck> <view> <say> <tackle> <pointto> <attention>)
  //    )]*
  // )
  var rootNode = JaSMIn.SymbolTreeParser.parse(line);

  if (rootNode.values.length > 1) {

    // Add partial state if valid
    storage.partialState.appendTo(sserverLog.states);
    storage.partialState.setGameTime(parseInt(rootNode.values[1], 10) / 10);

    var childNode
    var newAgent;

    // Parse symbol tree into partial world state of sserver log
    for (var i = 0; i < rootNode.children.length; i++) {
      childNode = rootNode.children[i];

      if (childNode.children.length > 0) {
        // Either a ball or player info
        if (childNode.children[0].values[0] === 'b') {
          // Found ball info
          JaSMIn.ULGParser.parseBallState(childNode, storage.partialState);
        } else if (childNode.children[0].values[0] === 'l') {
          // Found left team player
          JaSMIn.ULGParser.parseAgentState(childNode, sserverLog, sserverLog.leftTeam, storage.partialState.leftAgentStates);
        } else if (childNode.children[0].values[0] === 'r') {
          // Found right team player
          JaSMIn.ULGParser.parseAgentState(childNode, sserverLog, sserverLog.rightTeam, storage.partialState.rightAgentStates);
        } else {
          console.log('Found unexpected node in show line: ' + line.slice(0, 20));
        }
      } else if (childNode.values.length > 0) {
        // Either a playmode or team info
        if (childNode.values[0] === 'pm') {
          // parse the playmode number
          console.log('Found pm info in show line...');
        } else if (childNode.values[0] === 'tm') {
          // parse the team and scoring information
          console.log('Found tm info in show line...');
        } else {
          console.log('Found unexpected node in show line: ' + line.slice(0, 20));
        }
      } else {
        console.log('Found empty node in show line: ' + line.slice(0, 20));
      }
    }
  } else {
    console.log('Invalid show line: ' + line.slice(0, 20));
  }
};



/**
 * [parseBallState description]
 *
 * @param  {!JaSMIn.SymbolNode} node the ball symbol node
 * @param  {?JaSMIn.PartialWorldState} partialState the partial world state
 * @return {void}
 */
JaSMIn.ULGParser.parseBallState = function(node, partialState) {
  // ((b) <x> <y> <vx> <vy>)

  if (partialState === null || node.values.length < 2) {
    // Not enough data!
    return;
  }

  partialState.ballState = new JaSMIn.ObjectState(
      new THREE.Vector3(
            parseFloat(node.values[0]),
            0.2,
            parseFloat(node.values[1])),
      new THREE.Quaternion()
    );
};



/**
 * [parseAgentState description]
 *
 * @param  {!JaSMIn.SymbolNode} node the ball symbol node
 * @param  {!JaSMIn.SServerLog} sserverLog the ssserver log
 * @param  {!JaSMIn.TeamDescription} teamDescription the team description
 * @param  {!Array<!JaSMIn.AgentState>} teamStates the team agent states list
 * @return {void}
 */
JaSMIn.ULGParser.parseAgentState = function(node, sserverLog, teamDescription, teamStates) {
  // ((<side> <unum>) <player_type> <flags> <x> <y> <vx> <vy> <body> <neck> [<point-x> <point-y>]
  //   (v <view-quality> <view-width>)
  //   (s <stamina> <effort> <recovery> [<capacity>])
  //   [(f <side> <unum>)]
  //   (c <kick> <dash> <turn> <catch> <move> <tneck> <view> <say> <tackle> <pointto> <attention>)
  // )

  if (node.values.length < 7) {
    // Invalid agent node
    console.log('Expected more values in agent node: ' + node.values);
    return;
  }

  var playerNo = parseInt(node.children[0].values[1], 10);
  var typeIdx = parseInt(node.values[0], 10);
  var flags = parseInt(node.values[1], 16);

  teamDescription.addAgent(playerNo, sserverLog.playerTypes[typeIdx]);

  // Parse player state data
  var position = null;
  var quat = null;
  var jointData = [];
  var agentData = [];

  var angle = 0;
  var values;

  position = new THREE.Vector3(parseFloat(node.values[2]), 0, parseFloat(node.values[3]));
  angle = parseFloat(node.values[6]);
  quat = new THREE.Quaternion();
  quat.setFromAxisAngle(JaSMIn.Vector3_unitY, -angle * JaSMIn.PIby180);

  if (node.values.length > 7) {
    angle = parseFloat(node.values[7]);
    jointData[0] = -angle * JaSMIn.PIby180;
  }

  // TODO: Parse stamina, focus and count information
  for (var i = 1; i < node.children.length; i++) {
    values = node.children[i].values;

    if (values.length > 0) {
      if (values[0] === 'v') {
        // Parse view info
        // (v <view-quality> <view-width>)
      } else if (values[0] === 's') {
        // Parse stamina info
        // (s <stamina> <effort> <recovery> [<capacity>])
        if (values.length > 1) {
          agentData[JaSMIn.D2.AgentData.STAMINA] = parseFloat(values[1]);
        }
        if (values.length > 2) {
          agentData[JaSMIn.D2.AgentData.STAMINA_EFFORT] = parseFloat(values[2]);
        }
        if (values.length > 3) {
          agentData[JaSMIn.D2.AgentData.STAMINA_RECOVERY] = parseFloat(values[3]);
        }
        if (values.length > 4) {
          agentData[JaSMIn.D2.AgentData.STAMINA_CAPACITY] = parseFloat(values[4]);
        }
      } else if (values[0] === 'f') {
        // Parse focus info
        // (f <side> <unum>)
        if (values.length > 2) {
          agentData[JaSMIn.D2.AgentData.FOCUS_SIDE] = values[1] === 'l' ? 'l' : 'r';
          agentData[JaSMIn.D2.AgentData.FOCUS_UNUM] = parseInt(values[2], 10);
        } else {
          console.log('Found unexpected focus node in agent node!');
        }
      } else if (values[0] === 'c') {
        // Parse count info
        // (c <kick> <dash> <turn> <catch> <move> <tneck> <view> <say> <tackle> <pointto> <attention>)
        if (values.length > 1) {
          agentData[JaSMIn.D2.AgentData.KICK_COUNT] = parseInt(values[1], 10);
        }
        if (values.length > 2) {
          agentData[JaSMIn.D2.AgentData.DASH_COUNT] = parseInt(values[2], 10);
        }
        if (values.length > 3) {
          agentData[JaSMIn.D2.AgentData.TURN_COUNT] = parseInt(values[3], 10);
        }
        if (values.length > 4) {
          agentData[JaSMIn.D2.AgentData.CATCH_COUNT] = parseInt(values[4], 10);
        }
        if (values.length > 5) {
          agentData[JaSMIn.D2.AgentData.MOVE_COUNT] = parseInt(values[5], 10);
        }
        if (values.length > 6) {
          agentData[JaSMIn.D2.AgentData.TURN_NECK_COUNT] = parseInt(values[6], 10);
        }
        if (values.length > 7) {
          agentData[JaSMIn.D2.AgentData.VIEW_COUNT] = parseInt(values[7], 10);
        }
        if (values.length > 8) {
          agentData[JaSMIn.D2.AgentData.SAY_COUNT] = parseInt(values[8], 10);
        }
        if (values.length > 9) {
          agentData[JaSMIn.D2.AgentData.TACKLE_COUNT] = parseInt(values[9], 10);
        }
        if (values.length > 10) {
          agentData[JaSMIn.D2.AgentData.POINT_TO_COUNT] = parseInt(values[10], 10);
        }
        if (values.length > 11) {
          agentData[JaSMIn.D2.AgentData.ATTENTION_COUNT] = parseInt(values[11], 10);
        }
      } else {
        // Unknown subnode
        console.log('Found unexpected child node in agent node!');
      }
    } else {
      console.log('Found unexpected child node in agent node!');
    }
  }

  teamStates[playerNo] = new JaSMIn.AgentState(teamDescription.getRecentTypeIdx(playerNo), flags, position, quat, jointData, agentData);
};



/**
 * @param  {!string} line the server params line
 * @param  {!JaSMIn.SServerLog} sserverLog the ssserver log
 * @param  {!JaSMIn.LogParserStorage} storage the parser storage instance
 * @return {void}
 */
JaSMIn.ULGParser.parseServerParamLine = function(line, sserverLog, storage) {
  // (server_param (<name> <value>)*)
  sserverLog.environmentParams.clear();
  JaSMIn.ULGParser.parseParameters(line, sserverLog.environmentParams.paramObj, 'server parameter');

  // Update sserver log frequency and partial state time step
  sserverLog.updateFrequency();
  if (storage.partialState !== null) {
    storage.partialState.timeStep = 1 / sserverLog.frequency;
  }
};



/**
 * @param  {!string} line the player params line
 * @param  {!JaSMIn.SServerLog} sserverLog the ssserver log
 * @return {void}
 */
JaSMIn.ULGParser.parsePlayerParamLine = function(line, sserverLog) {
  // (player_param (<name> <value>)*)
  sserverLog.playerParams.clear();
  JaSMIn.ULGParser.parseParameters(line, sserverLog.playerParams.paramObj, 'player parameter');
};



/**
 * @param  {!string} line the player type line
 * @param  {!JaSMIn.SServerLog} sserverLog the ssserver log
 * @return {void}
 */
JaSMIn.ULGParser.parsePlayerTypeLine = function(line, sserverLog) {
  // (player_type (<name> <value>)*)
  var playerType = {};
  JaSMIn.ULGParser.parseParameters(line, playerType, 'player type');

  var typeIdx = /** @type {!number}*/ (playerType[JaSMIn.D2.PlayerTypeParams.ID]);
  if (typeIdx !== undefined) {
    sserverLog.playerTypes[typeIdx] = new JaSMIn.ParameterMap(playerType);
  }
};



/**
 * Parse a parameter line.
 *
 * @param  {!string} line the data line
 * @param  {!Object} params the target parameter object
 * @param  {!string} context the parameter context (for logging)
 * @return {void}
 */
JaSMIn.ULGParser.parseParameters = function(line, params, context) {
  var rootNode = JaSMIn.SymbolTreeParser.parse(line);
  var values;

  // Iterate over all param-value child nodes
  for (var i = 0; i < rootNode.children.length; i++) {
    values = rootNode.children[i].values;

    if (values.length < 2) {
      console.log('Malformated name-value pair in ' + context + ' line: ' + rootNode.children[i]);
      continue;
    }

    if (values[1] === 'true') {
      // Parse as boolean value
      params[values[0]] = true;
    } else if (values[1] === 'false') {
      // Parse as boolean value
      params[values[0]] = false;
    } else if (values[1].charAt(0) === '"') {
      // Parse as string value
      params[values[0]] = JaSMIn.copyString(values[1].slice(1, -1));
    } else {
      // Try parse as numerical value
      try {
        params[values[0]] = parseFloat(values[1]);
      } catch (ex) {
        // If parsing as numerical values fails, simply copy the whole string value
        params[values[0]] = JaSMIn.copyString(values[1]);
      }
    }
  }
};



/**
 * @param  {!string} line the message line
 * @param  {!JaSMIn.SServerLog} sserverLog the ssserver log
 * @return {void}
 */
JaSMIn.ULGParser.parseMessageLine = function(line, sserverLog) {

};



/**
 * @param  {!string} line the draw line
 * @param  {!JaSMIn.SServerLog} sserverLog the ssserver log
 * @return {void}
 */
JaSMIn.ULGParser.parseDrawLine = function(line, sserverLog) {

};
