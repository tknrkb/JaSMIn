/**
 * The GameLogLoader class definition.
 *
 * The GameLogLoader provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.GameLogLoader');
goog.provide('JaSMIn.GameLogLoaderEvents');

goog.require('JaSMIn');
goog.require('JaSMIn.EventDispatcher');
goog.require('JaSMIn.GameLog');
goog.require('JaSMIn.GameLogParser');
goog.require('JaSMIn.ReplayParser');
goog.require('JaSMIn.ULGParser');



/**
 * @enum {!string}
 */
JaSMIn.GameLogLoaderEvents = {
  START: 'start',
  NEW_GAME_LOG: 'new-game-log',
  PROGRESS: 'progress',
  FINISHED: 'finished',
  ERROR: 'error'
};






/**
 * [GameLogLoader description]
 *
 * @constructor
 * @implements {JaSMIn.IPublisher}
 * @implements {JaSMIn.IEventDispatcher}
 */
JaSMIn.GameLogLoader = function() {

  /**
   * The game log parser instance.
   * @type {?JaSMIn.GameLogParser}
   */
  this.parser = null;

  /**
   * The XMLHttpRequest object used to load remote game log files.
   * @type {?XMLHttpRequest}
   */
  this.xhr = null;

  /**
   * The FileReader object used to load the local game log files.
   * @type {?FileReader}
   */
  this.fileReader = null;



  /** @type {!Function} */
  this.xhrOnLoadListener = this.xhrOnLoad.bind(this);
  /** @type {!Function} */
  this.xhrOnProgressListener = this.xhrOnProgress.bind(this);
  /** @type {!Function} */
  this.xhrOnErrorListener = this.xhrOnError.bind(this);

  /** @type {!Function} */
  this.fileReaderOnLoadEndListener = this.fileReaderOnLoadEnd.bind(this);
};



/**
 * Load a game log from the specified url.
 *
 * @param  {!string} url the URL to the game log file
 * @return {void}
 */
JaSMIn.GameLogLoader.prototype.load = function(url) {
  // Clear loader instance
  this.clear();

  // Create a parser instance
  if (!this.createParserFor(url, true)) {
    return;
  }

  // Publish start event
  this.dispatchEvent({
    type: JaSMIn.GameLogLoaderEvents.START,
    url: url
  });

  // Create Request
  this.xhr = new XMLHttpRequest();
  this.xhr.open('GET', url, true);

  // Add event listeners
  this.xhr.addEventListener('load', this.xhrOnLoadListener, false);
  this.xhr.addEventListener('progress', this.xhrOnProgressListener, false);
  this.xhr.addEventListener('error', this.xhrOnErrorListener, false);

  // Set mime type
  if (this.xhr.overrideMimeType) {
    this.xhr.overrideMimeType('text/plain');
  }

  // Send request
  this.xhr.send(null);
};



/**
 * Load a game log file from the local file system.
 *
 * @param  {!File} file the file to load
 * @return {void}
 */
JaSMIn.GameLogLoader.prototype.loadFile = function(file) {
  // Clear loader instance
  this.clear();

  // Create a parser instance
  if (!this.createParserFor(file.name)) {
    return;
  }

  if (this.fileReader === null) {
    this.fileReader = new FileReader();
    this.fileReader.addEventListener('loadend', this.fileReaderOnLoadEndListener, false);
  }

  // Publish start event
  this.dispatchEvent({
    type: JaSMIn.GameLogLoaderEvents.START,
    url: file.name
  });

  // Read file
  // this.fileReader.readAsBinaryString(file);
  this.fileReader.readAsText(file);
};



/**
 * Load a game log file from the local file system.
 *
 * @param  {!string} name the fiel name / url / etc.
 * @param  {!boolean=} gzipAllowed indicator if gzipped file versions are accepted
 * @return {!boolean}
 */
JaSMIn.GameLogLoader.prototype.createParserFor = function(name, gzipAllowed) {
  if (JaSMIn.isSServerLogFile(name, gzipAllowed)) {
    // Try ulg parser
    this.parser = new JaSMIn.ULGParser();
  } else if (JaSMIn.isReplayFile(name, gzipAllowed)) {
    // Use replay parser
    this.parser = new JaSMIn.ReplayParser();
  } else {
    this.dispatchEvent({
        type: JaSMIn.GameLogLoaderEvents.ERROR,
        msg: 'Error while loading file! Failed to create game log parser!'
      });
  }

  return this.parser !== null;
};



/**
 * Clear the loader instance.
 *
 * @param {!boolean=} keepIteratorAlive indicator if iterator should not be disposed
 * @return {void}
 */
JaSMIn.GameLogLoader.prototype.clear = function(keepIteratorAlive) {
  if (this.xhr !== null) {
    // Remove event listeners
    this.xhr.removeEventListener('load', this.xhrOnLoadListener);
    this.xhr.removeEventListener('progress', this.xhrOnProgressListener);
    this.xhr.removeEventListener('error', this.xhrOnErrorListener);

    // Abort active request
    this.xhr.abort();
    this.xhr = null;
  }

  // TODO: Clear file loader instance

  if (this.parser !== null) {
    this.parser.dispose(keepIteratorAlive);
  }

  this.parser = null;
};



/**
 * The XHR onLoad callback.
 *
 * @param  {!Event} event the xhr event
 * @return {void}
 */
JaSMIn.GameLogLoader.prototype.xhrOnLoad = function(event) {
  if (event.target.status === 200 || event.target.status === 0) {
    // Parse remaining response
    this.parse(event.target.response, JaSMIn.DataExtent.COMPLETE);

    // Dispatch finished event
    this.dispatchEvent({
        type: JaSMIn.GameLogLoaderEvents.FINISHED
      });
  } else {
    // Error during loading
    this.dispatchEvent({
        type: JaSMIn.GameLogLoaderEvents.ERROR,
        msg: this.getXHRErrorMessage()
      });
  }
};



/**
 * The FileReader onLoadEnd callback.
 *
 * @param  {!Event} event the FileReader event
 * @return {void}
 */
JaSMIn.GameLogLoader.prototype.fileReaderOnLoadEnd = function(event) {
  var success = false;

  if (event.target.readyState == FileReader.DONE) { // DONE == 2
    // Successfully loaded
    success = true;

    // Parse file content
    this.parse(event.target.result, JaSMIn.DataExtent.COMPLETE);

    // Dispatch finished event
    this.dispatchEvent({
        type: JaSMIn.GameLogLoaderEvents.FINISHED
      });
  } else {
    // Clear loader instance
    this.clear();

    // Error during loading
    this.dispatchEvent({
        type: JaSMIn.GameLogLoaderEvents.ERROR,
        msg: 'Loading file failed!'
      });
  }
};



/**
 * The XHR onProgress callback.
 *
 * @param  {!Event} event the xhr event
 * @return {void}
 */
JaSMIn.GameLogLoader.prototype.xhrOnProgress = function(event) {
  // Dispatch progress event
  this.dispatchEvent({
      type: JaSMIn.GameLogLoaderEvents.PROGRESS,
      total: event.total,
      loaded: event.loaded
    });

  if (event.target.status === 200 || event.target.status === 0) {
    this.parse(event.target.response, JaSMIn.DataExtent.PARTIAL);
  }
};



/**
 * The XHR onError callback.
 *
 * @param  {!Event} event the xhr event
 * @return {void}
 */
JaSMIn.GameLogLoader.prototype.xhrOnError = function(event) {
  // Dispatch errer event
  this.dispatchEvent({
      type: JaSMIn.GameLogLoaderEvents.ERROR,
      msg: this.getXHRErrorMessage()
    });
};



/**
 * Try or continue parsing a game log.
 *
 * @param  {?string} data the current data
 * @param  {!JaSMIn.DataExtent} extent the data extent (complete, partial, incremental)
 * @return {void}
 */
JaSMIn.GameLogLoader.prototype.parse = function(data, extent) {
  if (!data || this.parser === null) {
    // Nothing to parse
    return;
  }

  try {
    if (this.parser.parse(data, extent)) {
      // A new game log instance was successfully created
      this.dispatchEvent({
          type: JaSMIn.GameLogLoaderEvents.NEW_GAME_LOG,
          gameLog: this.parser.getGameLog()
        });
    }

    if (extent === JaSMIn.DataExtent.COMPLETE) {
      // Clear loader instance
      this.clear(true);
    }
  } catch (ex) {
    // Clear loader instance
    this.clear();

    // Dispatch errer event
    this.dispatchEvent({
        type: JaSMIn.GameLogLoaderEvents.ERROR,
        msg: ex.toString()
      });
  }
};



/**
 * Retrieve the error message of the active XHR object, or create some default message if there is no error message available.
 *
 * @return {!string} the error/status message
 */
JaSMIn.GameLogLoader.prototype.getXHRErrorMessage = function() {
  var message = 'No active XMLHttpRequest to check for an error!';

  if (this.xhr !== null) {
    message = this.xhr.statusText;

    if (!message || message === '') {
      message = 'Unknown reason!';
    }
  }

  // Clear loader instance
  this.clear();

  return message;
};




// ============================== EVENT DISPATCHER FUNCTIONS ==============================
/** @override */
JaSMIn.GameLogLoader.prototype.addEventListener = JaSMIn.EventDispatcher.prototype.addEventListener;
/** @override */
JaSMIn.GameLogLoader.prototype.removeEventListener = JaSMIn.EventDispatcher.prototype.removeEventListener;
/** @override */
JaSMIn.GameLogLoader.prototype.dispatchEvent = JaSMIn.EventDispatcher.prototype.dispatchEvent;
// ============================== EVENT DISPATCHER FUNCTIONS ==============================
