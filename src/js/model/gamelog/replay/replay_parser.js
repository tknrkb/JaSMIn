/**
 * The ReplayParser class definition.
 *
 * The ReplayParser provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.ReplayParser');

goog.require('JaSMIn');
goog.require('JaSMIn.AgentDescription');
goog.require('JaSMIn.AgentState');
goog.require('JaSMIn.DataIterator');
goog.require('JaSMIn.LogParserStorage');
goog.require('JaSMIn.ObjectState');
goog.require('JaSMIn.ParserException');
goog.require('JaSMIn.PartialWorldState');
goog.require('JaSMIn.Replay');
goog.require('JaSMIn.TeamDescription');
goog.require('JaSMIn.WorldState');





/**
 * ReplayParser Constructor
 * Create a new ULG parser instance.
 *
 * @constructor
 * @implements {JaSMIn.GameLogParser}
 */
JaSMIn.ReplayParser = function() {

  /**
   * The replay data iterator.
   * @type {?JaSMIn.DataIterator}
   */
  this.iterator = null;

  /**
   * The replay.
   * @type {?JaSMIn.Replay}
   */
  this.replay = null;

  /**
   * The storage instance used during parsing.
   * @type {?JaSMIn.LogParserStorage}
   */
  this.storage = null;

  // console.log('New Replay parser instance created!');
};



/**
 * @override
 */
JaSMIn.ReplayParser.prototype.parse = function(data, extent) {
  if (this.iterator === null || this.replay === null || this.storage === null) {
    // Start parsing
    this.iterator = new JaSMIn.DataIterator(data, extent !== undefined ? extent : JaSMIn.DataExtent.COMPLETE);

    // ==================== Parse Replay Header ====================
    var line = this.iterator.next();
    if (line === null) {
      throw new JaSMIn.ParserException('Replay corrupt!');
    }
    var splitLine = line.split(' ');

    if (line.charAt(0) === 'R' &&
        line.charAt(1) === 'P' &&
        line.charAt(2) === 'L') {
      // Found replay header
      if (splitLine.length < 3) {
        throw new JaSMIn.ParserException('Malformated Replay Header!');
      }

      var type = splitLine[1] === '2D' ? JaSMIn.GameType.TWOD : JaSMIn.GameType.THREED;
      this.replay = new JaSMIn.Replay(type, parseInt(splitLine[2], 10));

      this.iterator.next();
    } else {
      // No replay header found, try fallback...
      if (line.charAt(0) === 'T') {
        // Initial 2D replay format, use fallback parser
        console.log('ReplayParser: Detected old 2D replay file format!');

        // Parse teams line
        if (splitLine.length < 3) {
          throw new JaSMIn.ParserException('Invalid team line!');
        }

        this.replay = new JaSMIn.Replay(JaSMIn.GameType.TWOD, 0);
        this.replay.leftTeam.setName(JaSMIn.copyString(splitLine[1].slice(1, -1)));
        this.replay.rightTeam.setName(JaSMIn.copyString(splitLine[2].slice(1, -1)));

        // Progress to next line
        this.iterator.next();

        // Create default agents with numbers 1 to 11 for both sides
        for (var i = 1; i < 12; i++) {
          this.replay.leftTeam.addAgent(i, this.replay.playerTypes[0]);
          this.replay.rightTeam.addAgent(i, this.replay.playerTypes[0]);
        }
      } else if (line.charAt(0) === 'V') {
        // Initial 3D replay format, use fallback parser
        console.log('ReplayParser: Detected old 3D replay file format!');

        if (splitLine.length < 4) {
          throw new JaSMIn.ParserException('Malformated Replay Header!');
        }

        this.replay = new JaSMIn.Replay(JaSMIn.GameType.THREED, 0);
        this.replay.frequency = parseInt(splitLine[3], 10);

        // Parse teams line
        line = this.iterator.next();
        if (line === null) {
          throw new JaSMIn.ParserException('Replay corrupt!');
        }
        splitLine = line.split(' ');
        if (splitLine.length < 5 || splitLine[0] != 'T') {
          throw new JaSMIn.ParserException('Invalid teams line!');
        }

        this.replay.leftTeam.setName(JaSMIn.copyString(splitLine[1].slice(1, -1)));
        this.replay.rightTeam.setName(JaSMIn.copyString(splitLine[3].slice(1, -1)));
        try {
          this.replay.leftTeam.color = new THREE.Color(splitLine[2]);
          this.replay.rightTeam.color = new THREE.Color(splitLine[4]);
        } catch (ex) {
          console.log(ex);
        }

        // Parse world line
        line = this.iterator.next();
        if (line === null) {
          throw new JaSMIn.ParserException('Replay corrupt!');
        }
        splitLine = line.split(' ');
        if (splitLine.length < 2 || splitLine[0] != 'F') {
          throw new JaSMIn.ParserException('Invalid world line!');
        }

        // Extract field parameters based on server version
        switch (parseInt(splitLine[1], 10)) {
          case 62:
            this.replay.environmentParams = JaSMIn.D3.createEnvironmentParamsV62();
            break;
          case 63:
            this.replay.environmentParams = JaSMIn.D3.createEnvironmentParamsV63();
            break;
          case 64:
            this.replay.environmentParams = JaSMIn.D3.createEnvironmentParamsV64();
            break;
          case 66:
            this.replay.environmentParams = JaSMIn.D3.createEnvironmentParamsV66();
            break;
          default:
            break;
        }

        // Progress to next line
        this.iterator.next();
      } else {
        throw new JaSMIn.ParserException('Failed parsing replay file - no Replay header found (and none of the fallback options applies)!');
      }
    }

    this.storage = new JaSMIn.LogParserStorage();
    this.storage.maxStates = this.replay.type === JaSMIn.GameType.TWOD ? 300 : 50;

    JaSMIn.ReplayParser.parseReplayBody(this.iterator, this.replay, this.storage);
    return true;
  } else {
    // Progress parsing
    if (this.iterator.update(data, extent !== undefined ? extent : JaSMIn.DataExtent.COMPLETE)) {
      // console.log('Restarting replay parser...');
      JaSMIn.ReplayParser.parseReplayBody(this.iterator, this.replay, this.storage);
    }
    return false;
  }
};



/**
 * Retrieve the currently parsed game log.
 *
 * @return {?JaSMIn.GameLog} the (maybe partially) parsed game log
 */
JaSMIn.ReplayParser.prototype.getGameLog = function() {
  return this.replay;
};



/**
 * Dispose all resources referenced in this parser instance.
 *
 * @param {!boolean=} keepIteratorAlive indicator if iterator should not be disposed
 * @return {void}
 */
JaSMIn.ReplayParser.prototype.dispose = function(keepIteratorAlive) {
  // console.log('Dispose Replay parser instance (keep iterator: ' + keepIteratorAlive + ')');

  if (this.iterator !== null && !keepIteratorAlive) {
    this.iterator.dispose();
  }

  this.iterator = null;
  this.replay = null;
  this.storage = null;
};









// ============================================================================
// ============================== STATIC MEMBERS ==============================
// ============================================================================

/**
 * [parseReplayBody description]
 *
 * @param  {!JaSMIn.DataIterator} iterator the replay data iterator
 * @param  {!JaSMIn.Replay} replay the replay to store the parsed states
 * @param  {!JaSMIn.LogParserStorage} storage the parser storage instance
 * @return {void}
 */
JaSMIn.ReplayParser.parseReplayBody = function(iterator, replay, storage) {
  var dataLine = iterator.line;
  if (dataLine === null) {
    // Try to restart the iterator
    dataLine = iterator.next();
  }

  var newStatesCnt = 0;

  // Parsing functions
  var parseBallFcn = JaSMIn.ReplayParser.parseBallState_2D;
  var parseAgentFcn = JaSMIn.ReplayParser.parseAgentState_V0_2D;
  if (replay.type === JaSMIn.GameType.THREED) {
    if (replay.version === 0) {
      parseBallFcn = JaSMIn.ReplayParser.parseBallState_V0_3D;
      parseAgentFcn = JaSMIn.ReplayParser.parseAgentState_V0_3D;
    } else {
      parseBallFcn = JaSMIn.ReplayParser.parseBallState_V1_3D;
      parseAgentFcn = JaSMIn.ReplayParser.parseAgentState_V1;
    }
  } else if (replay.version > 0) {
    parseAgentFcn = JaSMIn.ReplayParser.parseAgentState_V1;
  }

  while (dataLine !== null && newStatesCnt < storage.maxStates) {
    try {
      switch (dataLine.charAt(0)) {
        case 'E': // Environment parameter line
          if (dataLine.charAt(1) === 'P') {
            JaSMIn.ReplayParser.parseEnvironmentParams(dataLine, replay, storage);
          }
          break;
        case 'P':
          if (dataLine.charAt(1) === 'P') {
            // Player parameter line
            JaSMIn.ReplayParser.parsePlayerParams(dataLine, replay);
          } else if (dataLine.charAt(1) === 'T') {
            // Player type line
            JaSMIn.ReplayParser.parsePlayerTypeParams(dataLine, replay);
          }
          break;
        case 'T': // Team info line
          JaSMIn.ReplayParser.parseTeamLine(dataLine, replay);
          break;
        case 'S': // State dataL line
          if (JaSMIn.ReplayParser.parseStateLine(dataLine, replay, storage)) {
            newStatesCnt++;
          }
          break;

        case 'b': // Ball data line
          parseBallFcn(dataLine, storage.partialState);
          break;

        case 'l': // Left agent data line
        case 'L':
          if (storage.hasPartialState()) {
            parseAgentFcn(dataLine, replay, storage, true);
          }
          break;

        case 'r': // Right agent data line
        case 'R':
          if (storage.hasPartialState()) {
            parseAgentFcn(dataLine, replay, storage, false);
          }
          break;
      }
    } catch (ex) {
    }

    dataLine = iterator.next();
  }

  // Refresh replay
  if (newStatesCnt > 0) {
    replay.onStatesUpdated();
  }

  // Start parsing job, parsing $maxStates world states per run
  if (dataLine !== null) {
    setTimeout(JaSMIn.ReplayParser.parseReplayBody, 1, iterator, replay, storage);
  } else if (iterator.extent === JaSMIn.DataExtent.COMPLETE) {
    iterator.dispose();

    if (storage.hasPartialState()) {
      // Push final state
      storage.partialState.appendTo(replay.states);
    }

    replay.finalize();
  }
};












// ----------------------------------------------------------------------------
// --------------------------------- GENERAL ----------------------------------
// ----------------------------------------------------------------------------

/**
 * Parse a environment parameter line.
 *
 * @param  {!string} dataLine the environment params line
 * @param  {!JaSMIn.Replay} replay the replay instance
 * @param  {!JaSMIn.LogParserStorage} storage the parser storage instance
 * @return {void}
 */
JaSMIn.ReplayParser.parseEnvironmentParams = function(dataLine, replay, storage) {
  // Environment-Parameter Line-Format:
  // EP <single-line-json>
  try {
    var newParams = /** @type {!Object} */ (JSON.parse(dataLine.slice(3)));
    replay.environmentParams.clear();
    replay.environmentParams.paramObj = newParams;
  } catch (ex) {
    console.log('Exception while parsing environment parameters:');
    console.log(ex);
  }

  // Update replay frequency and partial state time step
  replay.updateFrequency();
  if (storage.partialState !== null) {
    storage.partialState.timeStep = 1 / replay.frequency;
  }
};



/**
 * Parse a player parameter line.
 *
 * @param  {!string} dataLine the player params line
 * @param  {!JaSMIn.Replay} replay the replay instance
 * @return {void}
 */
JaSMIn.ReplayParser.parsePlayerParams = function(dataLine, replay) {
  // Player-Parameter Line-Format:
  // PP <single-line-json>
  try {
    var newParams = /** @type {!Object} */ (JSON.parse(dataLine.slice(3)));
    replay.playerParams.clear();
    replay.playerParams.paramObj = newParams;
  } catch (ex) {
    console.log('Exception while parsing player parameters:');
    console.log(ex);
  }
};



/**
 * Parse a player type parameter line.
 *
 * @param  {!string} dataLine the player params line
 * @param  {!JaSMIn.Replay} replay the replay instance
 * @return {void}
 */
JaSMIn.ReplayParser.parsePlayerTypeParams = function(dataLine, replay) {
  // Player-Type-Parameter Line-Format:
  // PT <id> <single-line-json>
  var idx = dataLine.indexOf(' ', 4);

  if (idx > 3 && idx < 10) {
    var typeIdx = parseInt(dataLine.slice(3, idx), 10);

    try {
      replay.playerTypes[typeIdx] = new JaSMIn.ParameterMap(/** @type {!Object} */ (JSON.parse(dataLine.slice(idx + 1))));
    } catch (ex) {
      console.log('Exception while parsing player type parameters:');
      console.log(ex);
    }
  }
};



/**
 * Parse a team info line.
 *
 * @param  {!string} dataLine the team info line
 * @param  {!JaSMIn.Replay} replay the replay to store the parsed states
 * @return {void}
 */
JaSMIn.ReplayParser.parseTeamLine = function(dataLine, replay) {
  // Teams-Line-Format:
  // T <left-team> <right-team>[ <left-color <right-color>]
  var line = dataLine.split(' ');
  if (line.length < 3) {
    // Not enough data!
    return;
  }

  var updated = false;

  if (replay.leftTeam.setName(JaSMIn.copyString(line[1]))
      | replay.rightTeam.setName(JaSMIn.copyString(line[2]))) {
    updated = true;
  }

  if (line.length > 4) {
    try {
      if (replay.leftTeam.setColor(new THREE.Color(line[3]))
          | replay.rightTeam.setColor(new THREE.Color(line[4]))) {
        updated = true;
      }
    } catch (ex) {
      console.log(ex);
    }
  }

  if (updated) {
    // Publish update of team information
    replay.onTeamsUpdated();
  }
};



/**
 * Parse a state info line.
 *
 * @param  {!string} dataLine the team info line
 * @param  {!JaSMIn.Replay} replay the replay to store the parsed states
 * @param  {!JaSMIn.LogParserStorage} storage the parser storage instance
 * @return {!boolean} true, if a new world state was created, false otherwise
 */
JaSMIn.ReplayParser.parseStateLine = function(dataLine, replay, storage) {
  // State-Line-Format:
  // S <game-time> <playmode> <score-left> <score-right>[ <penalty-score-left> <penalty-miss-left> <penalty-miss-right> <penalty-miss-right>]
  var line = dataLine.split(' ');
  if (line.length < 5) {
    // Not enough data!
    return false;
  }

  var newStateCreated = false;
  if (storage.partialState !== null) {
    newStateCreated = storage.partialState.appendTo(replay.states);
  } else {
    storage.partialState = new JaSMIn.PartialWorldState(0, 1 / replay.frequency, 0);
  }

  var gameTime = parseFloat(line[1]);
  if (replay.version === 0) {
    gameTime /= 10;
  }

  storage.partialState.setGameTime(gameTime);
  storage.partialState.setPlaymode(line[2]);

  if (line.length > 8) {
    storage.partialState.setScore(
        parseInt(line[3], 10),
        parseInt(line[4], 10),
        parseInt(line[5], 10),
        parseInt(line[6], 10),
        parseInt(line[7], 10),
        parseInt(line[8], 10)
      );
  } else {
    storage.partialState.setScore(parseInt(line[3], 10), parseInt(line[4], 10));
  }

  return newStateCreated;
};



/**
 * [parseBallState description]
 *
 * @param {!string} dataLine
 * @param {?JaSMIn.PartialWorldState} partialState
 * @return {void}
 */
JaSMIn.ReplayParser.parseBallState_2D = function(dataLine, partialState) {
  // Ball-Line-Format:
  // b <x> <y>
  var line = dataLine.split(' ');
  if (partialState === null || line.length < 3) {
    // Not enough data!
    return;
  }

  partialState.ballState = new JaSMIn.ObjectState(
      new THREE.Vector3(parseFloat(line[1]), 0.2, parseFloat(line[2])),
      new THREE.Quaternion());
};












// ----------------------------------------------------------------------------
// --------------------------------- VERSION 0 --------------------------------
// ----------------------------------------------------------------------------



/**
 * [parseBallState description]
 *
 * @param  {!string} dataLine
 * @param  {?JaSMIn.PartialWorldState} partialState
 * @return {void}
 */
JaSMIn.ReplayParser.parseBallState_V0_3D = function(dataLine, partialState) {
  // Ball-Line-Format:
  // b <x> <y> <z> <qx> <qy> <qz> <qw>
  var line = dataLine.split(' ');
  if (partialState === null || line.length < 8) {
    // Not enough data!
    return;
  }

  partialState.ballState = new JaSMIn.ObjectState(
      new THREE.Vector3(
        parseInt(line[1], 10) / 1000,
        parseInt(line[3], 10) / 1000,
        -parseInt(line[2], 10) / 1000),
      new THREE.Quaternion(
        parseInt(line[5], 10) / 1000,
        parseInt(line[7], 10) / 1000,
        -parseInt(line[6], 10) / 1000,
        parseInt(line[4], 10) / 1000)
    );
};



/**
 * [parseAgentState description]
 *
 * @param  {!string} dataLine the agent line
 * @param  {!JaSMIn.Replay} replay the replay to store the parsed states
 * @param  {!JaSMIn.LogParserStorage} storage the parser storage instance
 * @param  {!boolean} leftSide side indicator
 * @return {void}
 */
JaSMIn.ReplayParser.parseAgentState_V0_2D = function(dataLine, replay, storage, leftSide) {
  // Agent-Line-Format:
  // {l|L|r|R} <unum> <x> <y> <heading-angle>[ <neck-angle> <stamina>]
  var line = dataLine.split(' ');
  if (line.length < 5) {
    // Not enough data!
    return;
  }

  var playerNo = parseInt(line[1], 10);
  var flags = JaSMIn.D2.AgentFlags.STAND;

  // Check for goalie
  if (line[0] === 'L' || line[0] === 'R') {
    flags |= JaSMIn.D2.AgentFlags.GOALIE;
  }

  // Parse player state data
  var position = new THREE.Vector3(parseFloat(line[2]), 0, parseFloat(line[3]));
  var angle = parseFloat(line[4]);
  var quat = new THREE.Quaternion();
  quat.setFromAxisAngle(JaSMIn.Vector3_unitY, -angle * JaSMIn.PIby180);
  var jointData = [];
  var agentData = [];

  if (line.length > 6) {
    angle = parseFloat(line[5]) - angle;
    if (angle > 180) {
      angle -= 360;
    } else if (angle < -180) {
      angle += 360;
    }

    jointData[0] = -angle * JaSMIn.PIby180;
    agentData[JaSMIn.D2.AgentData.STAMINA] = parseFloat(line[6].slice(1));
  }

  var newState = new JaSMIn.AgentState(0, flags, position, quat, jointData, agentData);

  if (leftSide) {
    storage.partialState.leftAgentStates[playerNo] = newState;
  } else {
    storage.partialState.rightAgentStates[playerNo] = newState;
  }
};

/**
 * [parseAgentState description]
 *
 * @param  {!string} dataLine the agent line
 * @param  {!JaSMIn.Replay} replay the replay to store the parsed states
 * @param  {!JaSMIn.LogParserStorage} storage the parser storage instance
 * @param  {!boolean} leftSide side indicator
 * @return {void}
 */
JaSMIn.ReplayParser.parseAgentState_V0_3D = function(dataLine, replay, storage, leftSide) {
  // Agent-Line-Format:
  // {l|L|r|R} <unum>[ model] <x> <y> <z> <qx> <qy> <qz> <qr> [ <joint-angle>]*
  var line = dataLine.split(' ');
  if (line.length < 9) {
    // Not enough data!
    return;
  }

  var playerNo = parseInt(line[1], 10);
  var dataIdx = 2;
  var modelIdx = 0;
  var team;
  var indexList;
  var agentStates;

  if (leftSide) {
    team = replay.leftTeam;
    indexList = storage.leftIndexList;
    agentStates = storage.partialState.leftAgentStates;
  } else {
    team = replay.rightTeam;
    indexList = storage.rightIndexList;
    agentStates = storage.partialState.rightAgentStates;
  }

  // Check for model definition
  if (line[0] === 'L' || line[0] === 'R') {
    if (line.length < 10) {
      // Not enough data!
      return;
    }

    dataIdx++;

    var modelType = 0;
    try {
      modelType = parseInt(line[2].slice(-1), 10);
    } catch (err) {
    }

    team.addAgent(playerNo, replay.playerTypes[modelType]);
    indexList[playerNo] = team.getRecentTypeIdx(playerNo);
  }

  if (indexList[playerNo] !== undefined) {
    modelIdx = indexList[playerNo];
  }

  // Parse player state data
  var position = new THREE.Vector3(
        parseInt(line[dataIdx], 10) / 1000,
        parseInt(line[dataIdx + 2], 10) / 1000,
        -parseInt(line[dataIdx + 1], 10) / 1000);
  var quat = new THREE.Quaternion(
        parseInt(line[dataIdx + 4], 10) / 1000,
        parseInt(line[dataIdx + 6], 10) / 1000,
        -parseInt(line[dataIdx + 5], 10) / 1000,
        parseInt(line[dataIdx + 3], 10) / 1000);
  var jointData = [];
  dataIdx += 7;

  // Shuffle joint data
  // Old joint order: <head> <l-arm> <l-leg> <r-arm> <r-leg>
  // New joint order: <head> <r-arm> <l-arm> <r-leg> <l-leg>
  var i;
  var numLegJoints = line.length - dataIdx > 22 ? 7 : 6;
  var lArmData = [];
  var lLegData = [];

  for (i = 0; i < 2 && dataIdx < line.length; i++, dataIdx++) {
    jointData.push(parseFloat(line[dataIdx]) * JaSMIn.PIby180 / 100);
  }
  for (i = 0; i < 4 && dataIdx < line.length; i++, dataIdx++) {
    lArmData.push(parseFloat(line[dataIdx]) * JaSMIn.PIby180 / 100);
  }
  for (i = 0; i < numLegJoints && dataIdx < line.length; i++, dataIdx++) {
    lLegData.push(parseFloat(line[dataIdx]) * JaSMIn.PIby180 / 100);
  }
  for (i = 0; i < 4 && dataIdx < line.length; i++, dataIdx++) {
    jointData.push(parseFloat(line[dataIdx]) * JaSMIn.PIby180 / 100);
  }
  for (i = 0; i < lArmData.length; i++) {
    jointData.push(lArmData[i]);
  }
  for (i = 0; i < numLegJoints && dataIdx < line.length; i++, dataIdx++) {
    jointData.push(parseFloat(line[dataIdx]) * JaSMIn.PIby180 / 100);
  }
  for (i = 0; i < lLegData.length; i++) {
    jointData.push(lLegData[i]);
  }


  agentStates[playerNo] = new JaSMIn.AgentState(modelIdx, 0x00, position, quat, jointData, []);
};












// ----------------------------------------------------------------------------
// --------------------------------- VERSION 1 --------------------------------
// ----------------------------------------------------------------------------

/**
 * [parseBallState description]
 *
 * @param  {!string} dataLine
 * @param  {?JaSMIn.PartialWorldState} partialState
 * @return {void}
 */
JaSMIn.ReplayParser.parseBallState_V1_3D = function(dataLine, partialState) {
  // Ball-Line-Format:
  // b <x> <y> <z> <qx> <qy> <qz> <qw>
  var line = dataLine.split(' ');
  if (partialState === null || line.length < 8) {
    // Not enough data!
    return;
  }

  partialState.ballState = new JaSMIn.ObjectState(
      new THREE.Vector3(
        parseFloat(line[1]),
        parseFloat(line[3]),
        -parseFloat(line[2])),
      new THREE.Quaternion(
        parseFloat(line[5]),
        parseFloat(line[7]),
        -parseFloat(line[6]),
        parseFloat(line[4]))
    );
};



/**
 * [parseAgentState description]
 *
 * @param  {!string} dataLine the agent line
 * @param  {!JaSMIn.Replay} replay the replay to store the parsed states
 * @param  {!JaSMIn.LogParserStorage} storage the parser storage instance
 * @param  {!boolean} leftSide side indicator
 * @return {void}
 */
JaSMIn.ReplayParser.parseAgentState_V1 = function(dataLine, replay, storage, leftSide) {
  // Agent-Line-Format:
  // 2D:
  // {l|L|r|R} <unum>[ typeIdx] <flags> <x> <y> <heading-angle>[(j[ <joint-angle>]+)][(s <stamina>)]
  // 3D:
  // {l|L|r|R} <unum>[ typeIdx] <flags> <x> <y> <z> <qx> <qy> <qz> <qr>[(j[ <joint-angle>]+)][(s <stamina>)]
  var rootNode = JaSMIn.SymbolTreeParser.parse('(' + dataLine + ')');
  if (rootNode.values.length < 6) {
    // Not enough data!
    return;
  }

  var playerNo = parseInt(rootNode.values[1], 10);
  var dataIdx = 2;
  var modelIdx = 0;
  var team;
  var indexList;
  var agentStates;

  if (leftSide) {
    team = replay.leftTeam;
    indexList = storage.leftIndexList;
    agentStates = storage.partialState.leftAgentStates;
  } else {
    team = replay.rightTeam;
    indexList = storage.rightIndexList;
    agentStates = storage.partialState.rightAgentStates;
  }

  // Check for model definition
  if (rootNode.values[0] === 'L' || rootNode.values[0] === 'R') {
    team.addAgent(playerNo, replay.playerTypes[parseInt(rootNode.values[dataIdx], 10)]);
    indexList[playerNo] = team.getRecentTypeIdx(playerNo);
    dataIdx++;
  }

  if (indexList[playerNo] !== undefined) {
    modelIdx = indexList[playerNo];
  }

  var flags = parseInt(rootNode.values[dataIdx], 16);
  dataIdx++;

  // Parse player state data
  var position = new THREE.Vector3();
  var quat = new THREE.Quaternion();
  var jointData = [];
  var agentData = [];
  var angle = 0;
  var is2D = replay.type === JaSMIn.GameType.TWOD;

  if (is2D) {
    position.set(parseFloat(rootNode.values[dataIdx]), 0, parseFloat(rootNode.values[dataIdx + 1]));
    quat.setFromAxisAngle(JaSMIn.Vector3_unitY, parseFloat(rootNode.values[dataIdx + 2]) * JaSMIn.NegPIby180);
  } else {
    position.set(parseFloat(rootNode.values[dataIdx]),
                 parseFloat(rootNode.values[dataIdx + 2]),
                 -parseFloat(rootNode.values[dataIdx + 1]));
    quat.set(parseFloat(rootNode.values[dataIdx + 4]),
             parseFloat(rootNode.values[dataIdx + 6]),
             -parseFloat(rootNode.values[dataIdx + 5]),
             parseFloat(rootNode.values[dataIdx + 3]));
  }

  for (var i = 0; i < rootNode.children.length; i++) {
    switch (rootNode.children[i].values[0]) {
      case 'j':
        JaSMIn.ReplayParser.parseJointNode(rootNode.children[i], jointData, is2D);
        break;
      case 's':
        agentData[JaSMIn.D2.AgentData.STAMINA] = parseFloat(rootNode.children[i].values[1]);
        break;
      default:
        break;
    }
  }

  agentStates[playerNo] = new JaSMIn.AgentState(modelIdx, flags, position, quat, jointData, agentData);
};



/**
 * Parse a joint-angles symbol node into a joint-data array.
 *
 * @param  {!JaSMIn.SymbolNode} node the joint-angles symbol node
 * @param  {!Array<!number>} jointData the joint data list
 * @param  {!boolean=} convert indicator if joint angles sign should be negated
 * @return {void}
 */
JaSMIn.ReplayParser.parseJointNode = function(node, jointData, convert) {
  var factor = convert === true ? JaSMIn.NegPIby180 : JaSMIn.PIby180;

  for (var i = 1; i < node.values.length; i++) {
    jointData.push(parseFloat(node.values[i]) * factor);
  }
};
