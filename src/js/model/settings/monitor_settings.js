/**
 * The MonitorSettings class definition.
 *
 * The MonitorSettings provides access to all configuration objects.
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.MonitorSettings');

goog.require('JaSMIn.IConfiguration');
goog.require('JaSMIn.MonitorConfiguration');







/**
 * The monitor settings event type enum.
 * @enum {!string}
 */
JaSMIn.MonitorSettingsEvents = {
  CHANGE: 'change'
};



/**
 * MonitorSettings Constructor
 *
 * @constructor
 * @implements {JaSMIn.IPublisher}
 * @implements {JaSMIn.IEventDispatcher}
 */
JaSMIn.MonitorSettings = function() {

  /**
   * The remember configurations settings list.
   * @type {!Object<!boolean>}
   */
  this.rememberMap = {};

  /**
   * The remember all configurations indicator.
   * @type {!boolean}
   */
  this.rememberAll = false;

  /**
   * The general monitor configuration.
   * @type {!JaSMIn.MonitorConfiguration}
   */
  this.monitorConfig = new JaSMIn.MonitorConfiguration();


  // Restore user configuration from local storage
  this.restore();
};





/**
 * Restore the user configurations from local storage.
 *
 * @return {void}
 */
JaSMIn.MonitorSettings.prototype.restore = function() {
  // console.log('Restoring settings...');

  // Restore remember all setting
  var value = localStorage.getItem('rememberAll');
  if (value) {
    // console.log('Found rememberAll value: ' + value);
    this.rememberAll = value === 'true';
  }

  // Restore remember map
  value = localStorage.getItem('rememberMap');
  if (value) {
    // console.log('Found rememberMap value: ' + value);
    try {
      this.rememberMap = /** @type {!Object<!boolean>} */ (JSON.parse(value));
    } catch (ex) {
      console.log('Exception parsing remember map!');
      console.log(ex);
    }
  }

  // restore individual configs
  this.restoreConfig(this.monitorConfig);
};





/**
 * Restore the specified user configuration from local storage.
 *
 * @param {!JaSMIn.IConfiguration} config the config to restore
 * @return {void}
 */
JaSMIn.MonitorSettings.prototype.restoreConfig = function(config) {
  var value = localStorage.getItem(config.getID());

  if (value) {
    // Found valid configuration value
     config.fromJSONString(value);
  }
};



/**
 * Save/Store the user configurations to the local storage.
 *
 * @return {void}
 */
JaSMIn.MonitorSettings.prototype.save = function() {
  // console.log('Saving settings...');

  // Save configuration remembering settings
  localStorage.setItem('rememberAll', this.rememberAll);
  localStorage.setItem('rememberMap', JSON.stringify(this.rememberMap));

  // Save individual configs
  this.saveConfig(this.monitorConfig);
};



/**
 * Save/Store the specified user configuration to the local storage.
 *
 * @param {!JaSMIn.IConfiguration} config the config to save/store
 * @return {void}
 */
JaSMIn.MonitorSettings.prototype.saveConfig = function(config) {
  var id = config.getID();
  if (this.rememberAll || this.rememberMap[id]) {
    localStorage.setItem(id, config.toJSONString());
  } else {
    localStorage.removeItem(id);
  }
};



/**
 * Enable/Disable remember setting for a specific configuration.
 *
 * @param {!JaSMIn.IConfiguration} config the configuration in question
 * @param {!boolean} remember true if the specified config should be stored in the local storage, false otherwise
 * @return {void}
 */
JaSMIn.MonitorSettings.prototype.setRememberConfig = function(config, remember) {
  this.rememberMap[config.getID()] = remember;

  // Publish change event
  this.dispatchEvent({
      type: JaSMIn.MonitorSettingsEvents.CHANGE
    });
};



/**
 * Enable/Disable remember setting for a all configurations.
 *
 * @param {!boolean} remember true if all configurations should be stored in the local storage, false otherwise
 * @return {void}
 */
JaSMIn.MonitorSettings.prototype.setRememberAll = function(remember) {
  this.rememberAll = remember;

  // Publish change event
  this.dispatchEvent({
      type: JaSMIn.MonitorSettingsEvents.CHANGE
    });
};




// ============================== EVENT DISPATCHER FUNCTIONS ==============================
/** @override */
JaSMIn.MonitorSettings.prototype.addEventListener = JaSMIn.EventDispatcher.prototype.addEventListener;
/** @override */
JaSMIn.MonitorSettings.prototype.removeEventListener = JaSMIn.EventDispatcher.prototype.removeEventListener;
/** @override */
JaSMIn.MonitorSettings.prototype.dispatchEvent = JaSMIn.EventDispatcher.prototype.dispatchEvent;
// ============================== EVENT DISPATCHER FUNCTIONS ==============================
