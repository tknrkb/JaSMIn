/**
 * The IConfiguration interface definition.
 *
 * The IConfiguration
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.IConfiguration');

goog.require('JaSMIn');





/**
 * IConfiguration Constructor
 *
 * @interface
 */
JaSMIn.IConfiguration = function() {};





/**
 * Retrieve the configuration id.
 *
 * @return {!string} the configuration id
 */
JaSMIn.IConfiguration.prototype.getID = function() {};



/**
 * Retrieve a stringified version of this configuration for persistance.
 *
 * @return {!string} the stringified version of this configuration
 */
JaSMIn.IConfiguration.prototype.toJSONString = function() {};



/**
 * Restore this configuration from persistance string.
 *
 * @param  {!string} jsonString a stringified version of this configuration
 * @return {void}
 */
JaSMIn.IConfiguration.prototype.fromJSONString = function(jsonString) {};
