/**
 * The MonitorConfiguration class definition.
 *
 * The MonitorConfiguration provides
 *
 * @author Stefan Glaser / http://chaosscripting.net
 */
goog.provide('JaSMIn.MonitorConfiguration');
goog.provide('JaSMIn.MonitorConfigurationEvents');
goog.provide('JaSMIn.MonitorConfigurationProperties');







/**
 * The monitor configuration event type enum.
 * @enum {!string}
 */
JaSMIn.MonitorConfigurationEvents = {
  CHANGE: 'change'
};



/**
 * The monitor configuration property enum.
 * @enum {!string}
 */
JaSMIn.MonitorConfigurationProperties = {
  TEAM_COLORS_ENABLED: 'teamColorsEnabled',
  TEAM_COLOR_LEFT: 'teamColorLeft',
  TEAM_COLOR_RIGHT: 'teamColorRight',
  INTERPOLATE_STATES: 'interpolateStates',
  SHADOWS_ENABLED: 'shadowsEnabled',
  GL_INFO_ENABLED: 'glInfoEnabled'
};





/**
 * MonitorConfiguration Constructor
 *
 * @constructor
 * @implements {JaSMIn.IPublisher}
 * @implements {JaSMIn.IEventDispatcher}
 * @implements {JaSMIn.IConfiguration}
 */
JaSMIn.MonitorConfiguration = function() {

  /**
   * Use user defined team colors?
   * @type {!boolean}
   */
  this.teamColorsEnabled = false;

  /**
   * User defined color for the left team.
   * @type {!THREE.Color}
   */
  this.leftTeamColor = new THREE.Color('#cccc00');

  /**
   * User defined color for the right team.
   * @type {!THREE.Color}
   */
  this.rightTeamColor = new THREE.Color('#008fff');

  /**
   * Interpolate world states?
   * @type {!boolean}
   */
  this.interpolateStates = true;

  /**
   * Are shadows enabled?
   * @type {!boolean}
   */
  this.shadowsEnabled = false;

  /**
   * Show gl panel info?
   * @type {!boolean}
   */
  this.glInfoEnabled = false;
};





/**
 * @override
 */
JaSMIn.MonitorConfiguration.prototype.getID = function() {
  return 'monitorConfig';
};



/**
 * @override
 */
JaSMIn.MonitorConfiguration.prototype.toJSONString = function() {
  var obj = {};

  // Store properties
  obj[JaSMIn.MonitorConfigurationProperties.TEAM_COLORS_ENABLED] = this.teamColorsEnabled;
  obj[JaSMIn.MonitorConfigurationProperties.TEAM_COLOR_LEFT] = this.leftTeamColor.getHex();
  obj[JaSMIn.MonitorConfigurationProperties.TEAM_COLOR_RIGHT] = this.rightTeamColor.getHex();
  obj[JaSMIn.MonitorConfigurationProperties.INTERPOLATE_STATES] = this.interpolateStates;
  obj[JaSMIn.MonitorConfigurationProperties.SHADOWS_ENABLED] = this.shadowsEnabled;
  obj[JaSMIn.MonitorConfigurationProperties.GL_INFO_ENABLED] = this.glInfoEnabled;

  return JSON.stringify(obj);
};



/**
 * @override
 */
JaSMIn.MonitorConfiguration.prototype.fromJSONString = function(jsonString) {
  try {
    var obj = JSON.parse(jsonString);

    // Read values
    var value = obj[JaSMIn.MonitorConfigurationProperties.TEAM_COLORS_ENABLED];
    if (value !== undefined) {
      this.teamColorsEnabled = value;
    }

    value = obj[JaSMIn.MonitorConfigurationProperties.TEAM_COLOR_LEFT];
    if (value !== undefined) {
      this.leftTeamColor = new THREE.Color(value);
    }

    value = obj[JaSMIn.MonitorConfigurationProperties.TEAM_COLOR_RIGHT];
    if (value !== undefined) {
      this.rightTeamColor = new THREE.Color(value);
    }

    value = obj[JaSMIn.MonitorConfigurationProperties.INTERPOLATE_STATES];
    if (value !== undefined) {
      this.interpolateStates = value;
    }

    value = obj[JaSMIn.MonitorConfigurationProperties.SHADOWS_ENABLED];
    if (value !== undefined) {
      this.shadowsEnabled = value;
    }

    value = obj[JaSMIn.MonitorConfigurationProperties.GL_INFO_ENABLED];
    if (value !== undefined) {
      this.glInfoEnabled = value;
    }
  } catch (ex) {
    console.log(ex);
  }
};



/**
 * Enable/Disable usage of user defined team colors.
 *
 * @param {!boolean} value true for enabled, false for disabled
 * @return {void}
 */
JaSMIn.MonitorConfiguration.prototype.setTeamColorsEnabled = function(value) {
  if (this.teamColorsEnabled !== value) {
    this.teamColorsEnabled = value;

    // Publish change event
    this.dispatchEvent({
      type: JaSMIn.MonitorConfigurationEvents.CHANGE,
      property: JaSMIn.MonitorConfigurationProperties.TEAM_COLORS_ENABLED,
      newValue: value
    });
  }
};



/**
 * Store the given color as the user defined color for the left team.
 *
 * @param {!string} color the user defined team color
 * @param {!boolean} leftSide true if the color is for the left team, false for the right team
 * @return {void}
 */
JaSMIn.MonitorConfiguration.prototype.setTeamColor = function(color, leftSide) {
  if (leftSide) {
    this.leftTeamColor = new THREE.Color(color);

    // Publish change event
    this.dispatchEvent({
      type: JaSMIn.MonitorConfigurationEvents.CHANGE,
      property: JaSMIn.MonitorConfigurationProperties.TEAM_COLOR_LEFT,
      newValue: this.leftTeamColor
    });
  } else {
    this.rightTeamColor = new THREE.Color(color);

    // Publish change event
    this.dispatchEvent({
      type: JaSMIn.MonitorConfigurationEvents.CHANGE,
      property: JaSMIn.MonitorConfigurationProperties.TEAM_COLOR_RIGHT,
      newValue: this.rightTeamColor
    });
  }
};



/**
 * Read the user defined color for a team.
 *
 * @param  {!boolean} leftSide true for left side, false for right side
 * @return {!THREE.Color} the user defined team color
 */
JaSMIn.MonitorConfiguration.prototype.getTeamColor = function(leftSide) {
  return leftSide ? this.leftTeamColor : this.rightTeamColor;
};



/**
 * @param {!boolean} value
 */
JaSMIn.MonitorConfiguration.prototype.setInterpolateStates = function(value) {
  if (this.interpolateStates !== value) {
    this.interpolateStates = value;

    // Publish change event
    this.dispatchEvent({
      type: JaSMIn.MonitorConfigurationEvents.CHANGE,
      property: JaSMIn.MonitorConfigurationProperties.INTERPOLATE_STATES,
      newValue: value
    });
  }
};



/**
 * @param {!boolean} value
 */
JaSMIn.MonitorConfiguration.prototype.setShadowsEnabled = function(value) {
  if (this.shadowsEnabled !== value) {
    this.shadowsEnabled = value;

    // Publish change event
    this.dispatchEvent({
      type: JaSMIn.MonitorConfigurationEvents.CHANGE,
      property: JaSMIn.MonitorConfigurationProperties.SHADOWS_ENABLED,
      newValue: value
    });
  }
};



/**
 * @param {!boolean} value
 */
JaSMIn.MonitorConfiguration.prototype.setGLInfoEnabled = function(value) {
  if (this.glInfoEnabled !== value) {
    this.glInfoEnabled = value;

    // Publish change event
    this.dispatchEvent({
      type: JaSMIn.MonitorConfigurationEvents.CHANGE,
      property: JaSMIn.MonitorConfigurationProperties.GL_INFO_ENABLED,
      newValue: value
    });
  }
};




// ============================== EVENT DISPATCHER FUNCTIONS ==============================
/** @override */
JaSMIn.MonitorConfiguration.prototype.addEventListener = JaSMIn.EventDispatcher.prototype.addEventListener;
/** @override */
JaSMIn.MonitorConfiguration.prototype.removeEventListener = JaSMIn.EventDispatcher.prototype.removeEventListener;
/** @override */
JaSMIn.MonitorConfiguration.prototype.dispatchEvent = JaSMIn.EventDispatcher.prototype.dispatchEvent;
// ============================== EVENT DISPATCHER FUNCTIONS ==============================
