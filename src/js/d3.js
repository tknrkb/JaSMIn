goog.provide('JaSMIn.D3');

goog.require('JaSMIn');
goog.require('JaSMIn.ParameterMap');




/**
 * General 3D Simulation namespace, definitions, etc.
 *
 * @author Stefan Glaser
 */
JaSMIn.D3 = {};



/**
 * An enum providing meaning the indices for the different elements in the agent flags bitfield for 3D games.
 * @enum {!number}
 */
JaSMIn.D3.AgentFlags = {
    CROWDING:        0x00000001,
    TOUCHING:        0x00000002,
    ILLEGAL_DEFENCE: 0x00000004,
    ILLEGAL_ATTACK:  0x00000008,
    INCAPABLE:       0x00000010,
    ILLEGAL_KICKOFF: 0x00000020,
    CHARGING:        0x00000040,
};



/**
 * An enum providing known environment parameter names for 3D games.
 * @enum {!string}
 */
JaSMIn.D3.EnvironmentParams = {
  LOG_STEP: 'log_step',

  FIELD_LENGTH: 'FieldLength',
  FIELD_WIDTH: 'FieldWidth',
  FIELD_HEIGHT: 'FieldHeight',
  GOAL_WIDTH: 'GoalWidth',
  GOAL_DEPTH: 'GoalDepth',
  GOAL_HEIGHT: 'GoalHeight',
  BORDER_SIZE: 'BorderSize',
  FREE_KICK_DISTANCE: 'FreeKickDistance',
  WAIT_BEFORE_KICK_OFF: 'WaitBeforeKickOff',
  AGENT_MASS: 'AgentMass',
  AGENT_RADIUS: 'AgentRadius',
  AGENT_MAX_SPEED: 'AgentMaxSpeed',
  BALL_RADIUS: 'BallRadius',
  BALL_MASS: 'BallMass',
  RULE_GOAL_PAUSE_TIME: 'RuleGoalPauseTime',
  RULE_KICK_IN_PAUSE_TIME: 'RuleKickInPauseTime',
  RULE_HALF_TIME: 'RuleHalfTime',
  PLAY_MODES: 'play_modes'
};



/**
 * An enum providing known player parameter names for 3D games.
 * @enum {!string}
 */
JaSMIn.D3.PlayerParams = {
  NONE: 'none'
};



/**
 * An enum providing known player type parameter names for 3D games.
 * @enum {!string}
 */
JaSMIn.D3.PlayerTypeParams = {
  MODEL_NAME: 'model',
  MODEL_TYPE: 'model_type'
};






/**
 * Create a new environment parameters object with default values set for 3D games.
 *
 * @return {!JaSMIn.ParameterMap}
 */
JaSMIn.D3.createDefaultEnvironmentParams = function() {
  return JaSMIn.D3.createEnvironmentParamsV66();
};



/**
 * Create a new set of environment parameters for SimSpark version 62.
 *
 * @return {!JaSMIn.ParameterMap}
 */
JaSMIn.D3.createEnvironmentParamsV62 = function() {
  var params = {};

  params[JaSMIn.D3.EnvironmentParams.LOG_STEP] = 200;

  // Default parameters for version 66
  params[JaSMIn.D3.EnvironmentParams.FIELD_LENGTH] = 12;
  params[JaSMIn.D3.EnvironmentParams.FIELD_WIDTH] = 8;
  params[JaSMIn.D3.EnvironmentParams.FIELD_HEIGHT] = 40;
  params[JaSMIn.D3.EnvironmentParams.GOAL_WIDTH] = 1.4;
  params[JaSMIn.D3.EnvironmentParams.GOAL_DEPTH] = 0.4;
  params[JaSMIn.D3.EnvironmentParams.GOAL_HEIGHT] = 0.8;
  params[JaSMIn.D3.EnvironmentParams.FREE_KICK_DISTANCE] = 1;
  params[JaSMIn.D3.EnvironmentParams.AGENT_RADIUS] = 0.4;
  params[JaSMIn.D3.EnvironmentParams.BALL_RADIUS] = 0.042;
  params[JaSMIn.D3.EnvironmentParams.RULE_HALF_TIME] = 300;

  return new JaSMIn.ParameterMap(params);
};



/**
 * Create a new set of environment parameters for SimSpark version 63.
 *
 * @return {!JaSMIn.ParameterMap}
 */
JaSMIn.D3.createEnvironmentParamsV63 = function() {
  var paramMap = JaSMIn.D3.createEnvironmentParamsV62();

  // Default parameters for version 66
  paramMap.paramObj[JaSMIn.D3.EnvironmentParams.FIELD_LENGTH] = 18;
  paramMap.paramObj[JaSMIn.D3.EnvironmentParams.FIELD_WIDTH] = 12;

  paramMap.paramObj[JaSMIn.D3.EnvironmentParams.GOAL_WIDTH] = 2.1;
  paramMap.paramObj[JaSMIn.D3.EnvironmentParams.GOAL_DEPTH] = 0.6;

  paramMap.paramObj[JaSMIn.D3.EnvironmentParams.FREE_KICK_DISTANCE] = 1.8;

  return paramMap;
};



/**
 * Create a new set of environment parameters for SimSpark version 64.
 *
 * @return {!JaSMIn.ParameterMap}
 */
JaSMIn.D3.createEnvironmentParamsV64 = function() {
  var paramMap = JaSMIn.D3.createEnvironmentParamsV63();

  // Default parameters for version 66
  paramMap.paramObj[JaSMIn.D3.EnvironmentParams.FIELD_LENGTH] = 21;
  paramMap.paramObj[JaSMIn.D3.EnvironmentParams.FIELD_WIDTH] = 14;

  return paramMap;
};



/**
 * Create a new set of environment parameters for SimSpark version 66.
 *
 * @return {!JaSMIn.ParameterMap}
 */
JaSMIn.D3.createEnvironmentParamsV66 = function() {
  var paramMap = JaSMIn.D3.createEnvironmentParamsV63();

  // Default parameters for version 66
  paramMap.paramObj[JaSMIn.D3.EnvironmentParams.FIELD_LENGTH] = 30;
  paramMap.paramObj[JaSMIn.D3.EnvironmentParams.FIELD_WIDTH] = 20;

  paramMap.paramObj[JaSMIn.D3.EnvironmentParams.FREE_KICK_DISTANCE] = 2;

  return paramMap;
};



/**
 * Create a new player parameters object with default values set for 3D games.
 *
 * @return {!JaSMIn.ParameterMap}
 */
JaSMIn.D3.createDefaultPlayerParams = function() {
  var params = {};

  return new JaSMIn.ParameterMap(params);
};



/**
 * Create a new list of default player type parameter objects for 3D games.
 *
 * @return {!Array<!JaSMIn.ParameterMap>}
 */
JaSMIn.D3.createDefaultPlayerTypeParams = function() {
  var types = [];

  // Create nao_hetero0 to nao_hetero4 player types
  for (var i = 0; i < 5; i++) {
    types[i] = new JaSMIn.ParameterMap();
    types[i].paramObj[JaSMIn.D3.PlayerTypeParams.MODEL_NAME] = 'nao_hetero';
    types[i].paramObj[JaSMIn.D3.PlayerTypeParams.MODEL_TYPE] = i;
  }

  return types;
};
