var gulp = require('gulp'),
	gulpLoadPlugins = require('gulp-load-plugins'),
	gp = gulpLoadPlugins(),
  path = require('path'),
  compiler = require('google-closure-compiler-js').gulp();


var buildDir = path.join(__dirname, 'build');
var srcDir = path.join(__dirname, 'src');


gulp.task('build', ['build-js', 'build-sass', 'copy-textures', 'copy-models', 'copy-images', 'copy-examples']);


gulp.task('build-js', function () {
  return gulp.src(['node_modules/google-closure-library/closure/goog/base.js',
                   './src/js/externs.js',
                   // 'node_modules/three/build/three.js',
                   './src/js/threejs-externs.js',
                   './src/js/main.js',


                   // ----- General utils
                   './src/js/utils/exceptions/parser_exception.js',

                   './src/js/utils/event_dispatcher.js',
                   './src/js/utils/fps_meter.js',
                   './src/js/utils/game_log_info.js',
                   './src/js/utils/symboltree/symbol_node.js',
                   './src/js/utils/symboltree/symbol_tree_parser.js',
                   './src/js/utils/data_iterator.js',
                   // './src/js/utils/file_data_iterator.js',
                   './src/js/utils/parameter_map.js',


                   // ----- Namespaces and 2D/3D domain specs
                   './src/js/d2.js',
                   './src/js/d3.js',
                   './src/js/ui/ui.js',
                   './src/js/model/gl/gl.js',


                   // ----- Models
                   './src/js/model/settings/i_configuration.js',
                   './src/js/model/settings/monitor_configuration.js',
                   './src/js/model/settings/monitor_settings.js',

                   './src/js/model/gamelog/agent_description.js',
                   './src/js/model/gamelog/team_description.js',
                   './src/js/model/gamelog/object_state.js',
                   './src/js/model/gamelog/agent_state.js',
                   './src/js/model/gamelog/game_state.js',
                   './src/js/model/gamelog/game_score.js',
                   './src/js/model/gamelog/world_state.js',
                   './src/js/model/gamelog/game_log.js',

                   './src/js/model/gamelog/parser/partial_world_state.js',
                   './src/js/model/gamelog/parser/log_parser_storage.js',
                   './src/js/model/gamelog/parser/game_log_parser.js',

                   './src/js/model/gamelog/replay/replay.js',
                   './src/js/model/gamelog/replay/replay_parser.js',

                   './src/js/model/gamelog/sserver/sserver_log.js',
                   './src/js/model/gamelog/sserver/ulg_parser.js',

                   './src/js/model/gamelog/loader/game_log_loader.js',

                   './src/js/model/gl/utils/geometry_factory.js',
                   './src/js/model/gl/utils/json_geometry_factory.js',
                   './src/js/model/gl/utils/material_factory.js',
                   './src/js/model/gl/utils/mesh_factory.js',
                   './src/js/model/gl/utils/scene_utils.js',

                   './src/js/model/gl/world/movable_object.js',
                   './src/js/model/gl/world/ball.js',
                   './src/js/model/gl/world/robot_model.js',
                   './src/js/model/gl/world/agent.js',
                   './src/js/model/gl/world/team.js',
                   './src/js/model/gl/world/field.js',

                   './src/js/model/gl/world/robots/robot_model_factory.js',
                   './src/js/model/gl/world/robots/robot_specification.js',
                   './src/js/model/gl/world/robots/dynamic_robot_model.js',

                   './src/js/model/gl/world/robots/nao/nao_specification.js',
                   './src/js/model/gl/world/robots/nao/nao_model_factory.js',

                   './src/js/model/gl/world/robots/soccerbot2d/soccer_bot_2d_specification.js',
                   './src/js/model/gl/world/robots/soccerbot2d/soccer_bot_2d.js',
                   './src/js/model/gl/world/robots/soccerbot2d/soccer_bot_2d_model_factory.js',

                   './src/js/model/gl/world/loader/world_model_factory.js',
                   './src/js/model/gl/world/loader/world_loader.js',

                   './src/js/model/gl/world/world.js',

                   './src/js/model/logplayer/playlist.js',
                   './src/js/model/logplayer/playlist_loader.js',
                   './src/js/model/logplayer/log_player.js',

                   './src/js/model/monitor_model.js',


                   // ----- User interface
                   './src/js/ui/utils/dnd_handler.js',
                   './src/js/ui/utils/panel.js',
                   './src/js/ui/utils/overlay.js',
                   './src/js/ui/utils/panel_group.js',
                   './src/js/ui/utils/tab_pane.js',
                   './src/js/ui/utils/single_choice_item.js',
                   './src/js/ui/utils/toggle_item.js',
                   './src/js/ui/utils/fullscreen_manager.js',
                   './src/js/ui/utils/i_camera_controller.js',
                   './src/js/ui/utils/universal_camera_controller.js',
                   './src/js/ui/utils/gl_info_board.js',
                   './src/js/ui/utils/gl_panel.js',

                   // './src/js/ui/notification/notification_manager.js',

                   './src/js/ui/explorer/archive.js',
                   './src/js/ui/explorer/archive_explorer.js',
                   './src/js/ui/explorer/resource_explorer.js',

                   './src/js/ui/welcome_pane.js',
                   './src/js/ui/game_info_board.js',
                   './src/js/ui/loading_bar.js',

                   './src/js/ui/input_controller.js',

                   './src/js/ui/overlay/help_overlay.js',
                   './src/js/ui/overlay/info_overlay.js',
                   './src/js/ui/overlay/playlist_overlay.js',
                   './src/js/ui/overlay/settings_overlay.js',

                   './src/js/ui/player_ui.js',
                   './src/js/ui/monitor_ui.js',


                   // ----- General classes
                   './src/js/monitor_parameter.js',
                   './src/js/monitor.js'], {base: './'})
    .pipe(gp.sourcemaps.init())
    .pipe(compiler({
        jsOutputFile: 'JaSMIn.min.js',  // filename returned to gulp
        compilationLevel: 'ADVANCED',   // as opposed to 'SIMPLE', the default
        // compilationLevel: 'SIMPLE',   // as opposed to 'ADVANCED'
        warningLevel: 'VERBOSE',        // complain loudly on errors
        createSourceMap: true,          // create output source map
        processCommonJsModules: true,   // needed to support require()
        generateExports: true,
      }))
    .pipe(gp.sourcemaps.write('/'))
    .pipe(gulp.dest('./build/js/'));
});

gulp.task('build-sass', function () {
  return gulp.src(path.join(srcDir, 'scss/JaSMIn.scss'))
    .pipe(gp.sass({ style: 'expanded', sourceComments: 'map', errLogToConsole: true}))
    //.pipe(gp.autoprefixer('last 2 version', "> 1%", 'ie 8', 'ie 9'))
    .pipe(gulp.dest(path.join(buildDir, 'css')))
    .pipe(gp.notify({ message: 'LibSass files dropped!' }));
});

gulp.task('copy-textures', function () {
  return gulp.src(path.join(srcDir, 'textures/*'))
    .pipe(gulp.dest(path.join(buildDir, 'textures')));
});

gulp.task('copy-models', function () {
  return gulp.src(path.join(srcDir, 'blender/json/*'))
    .pipe(gulp.dest(path.join(buildDir, 'models')));
});

gulp.task('copy-images', function () {
  return gulp.src(path.join(srcDir, 'images/*'))
    .pipe(gulp.dest(path.join(buildDir, 'images')));
});

gulp.task('copy-examples', function () {
  return gulp.src('examples/**/*')
    .pipe(gulp.dest(buildDir));
});

gulp.task('clean', function() {
  return gulp.src('build/*', {read: false})
	 .pipe(gp.clean());
});




//  Default Gulp Task
//===========================================
gulp.task('default', ['build'], function() {

});


// Watch Files For Changes
//===========================================
gulp.task('watch-nojs', function() {
    gulp.watch(path.join(srcDir, 'scss/*.scss'), ['build-sass']);
    gulp.watch('examples/**/*', ['copy-examples']);
    gulp.watch(path.join(srcDir, 'images/*'), ['copy-images']);
    gulp.watch(path.join(srcDir, 'blender/json/*'), ['copy-models']);
    gulp.watch(path.join(srcDir, 'textures/*'), ['copy-textures']);
});
