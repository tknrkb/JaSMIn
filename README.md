# JaSMIn

Javascript Soccer Monitor Interface, a webgl based player for RoboCup Simulation replay files and [SServer](https://sourceforge.net/projects/sserver/) logs.


1. OVERVIEW
===================

JaSMIn is a webgl based player for displaying soccer games from RoboCup Soccer Simulation leagues (2D and 3D) in a browser. The current version of JaSMIn can replay the following log file formats:
- .replay files, converted SServer log files via the [GIBBS converter](https://github.com/OliverObst/GIBBS)
- .rpl3d files, converted SimSpark log files with my personal, still unpublished converter...
- .rcg text files (ULGv4/5), as currently logged by the SServer simulator

Due to the lack of TCP Socket support within browsers, JaSMIn can not connect directly to simulation servers until they provide a WebSocket interface. At some point in time you may be able to use JaSMIn to watch your simulated soccer matches live... but not today.



2. SETUP
===================

1. Clone repository
2. Install build dependencies via node.js:
  - `npm install`
3. Build JaSMIn (may take some time...):
  - `gulp`

After the build process finished successfully, you'll find two major landing pages in the build directory:
- player.html
- embedded.html (forwarding to embedded-player.html)

While the player.html runs a standalone version of JaSMIn, the embedded.html runs an embedded version with a reduced feature set.

For local testing purposes, you can run a webserver from within the repository root directory e.g. via php:

`php -S localhost:8080`

and then navigate your browser to: "localhost:8080/build/player.html".



3. DEPLOYMENT
===================

In order to deploy JaSMIn on your own server, you need to follow three simple steps:
1. Copy all files and directories within the build directory to your target location.
2. Copy the three.min.js library from the node_modules/three/build directory into the js folder of your target location.
3. Adapt script paths & functionality in player.html, embedded-player.html and archive.php.
